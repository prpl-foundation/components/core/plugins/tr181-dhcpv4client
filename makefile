include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C modules/common/src all
	$(MAKE) -C modules/mod-uci-access/src all
	$(MAKE) -C modules/mod-udhcpc-access/src all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean
	$(MAKE) -C modules/common/src clean
	$(MAKE) -C modules/mod-uci-access/src clean
	$(MAKE) -C modules/mod-udhcpc-access/src clean
	$(MAKE) -C modules/mod-uci-access/test clean
	$(MAKE) -C modules/mod-udhcpc-access/test clean

install: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-uci-access.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-uci-access.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-udhcpc-access.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-udhcpc-access.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 odl/dhcpv4c_definition.odl $(DEST)/etc/amx/$(COMPONENT)/dhcpv4c_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-dhcpv4client_mapping.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.gw.d
	$(foreach odl,$(wildcard odl/defaults.gw.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.gw.d/;)
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.ap.d
	$(foreach odl,$(wildcard odl/defaults.ap.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.ap.d/;)
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/$(COMPONENT)
	ln -sfr $(DEST)/etc/amx/$(COMPONENT)/defaults.gw.d $(DEST)/etc/amx/$(COMPONENT)/defaults.d
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/$(COMPONENT)
	ln -sfr $(DEST)/etc/amx/$(COMPONENT)/defaults.ap.d $(DEST)/etc/amx/$(COMPONENT)/defaults.d
endif
	$(INSTALL) -D -p -m 0755 scripts/01_environment_dhcpv4client.sh $(DEST)/etc/env.d/01_environment_dhcpv4client.sh
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/udhcpc.user $(DEST)/etc/udhcpc.user.d/tr181-dhcpv4client
	$(INSTALL) -D -p -m 0755 scripts/udhcpc-script.sh $(DEST)/etc/amx/$(COMPONENT)/udhcpc-script.sh
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-uci-access.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-uci-access.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-udhcpc-access.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-udhcpc-access.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 odl/dhcpv4c_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/dhcpv4c_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-dhcpv4client_mapping.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.gw.d
	$(INSTALL) -D -p -m 0644 odl/defaults.gw.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.gw.d/
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.ap.d
	$(INSTALL) -D -p -m 0644 odl/defaults.ap.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.ap.d/
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/$(COMPONENT)
	rm -f $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d
	ln -sfr $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.gw.d $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/$(COMPONENT)
	rm -f $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d
	ln -sfr $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.ap.d $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d
endif
	$(INSTALL) -D -p -m 0755 scripts/01_environment_dhcpv4client.sh $(PKGDIR)/etc/env.d/01_environment_dhcpv4client.sh
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/udhcpc.user $(PKGDIR)/etc/udhcpc.user.d/tr181-dhcpv4client
	$(INSTALL) -D -p -m 0755 scripts/udhcpc-script.sh $(PKGDIR)/etc/amx/$(COMPONENT)/udhcpc-script.sh
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/$(COMPONENT).odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C modules/mod-udhcpc-access/test run
	$(MAKE) -C modules/mod-udhcpc-access/test coverage
	$(MAKE) -C modules/mod-uci-access/test run
	$(MAKE) -C modules/mod-uci-access/test coverage
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test