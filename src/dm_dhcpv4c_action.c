/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <debug/sahtrace.h>

#include "dm_dhcpv4client.h"
#include "client_info.h"

static amxc_var_t* describe_statistic(amxc_var_t* ht_params,
                                      amxc_var_t* var) {
    amxc_var_t* value = NULL;
    amxc_var_t* param = NULL;

    param = amxc_var_add_key(amxc_htable_t, ht_params, amxc_var_key(var), NULL);

    amxd_param_build_description(param, amxc_var_key(var), var->type_id,
                                 SET_BIT(amxd_pattr_read_only) |
                                 SET_BIT(amxd_pattr_variable),
                                 NULL);

    value = GET_ARG(param, "value");
    amxc_var_set(uint32_t, value, GET_UINT32(var, NULL));

    return param;
}

static amxd_status_t get_value_statistics(amxc_var_t* const retval,
                                          const char* filter,
                                          amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxp_expr_t expr;
    amxc_var_t description;
    amxp_expr_status_t expr_status = amxp_expr_status_ok;
    int retv = -1;

    amxc_var_init(&description);
    amxc_var_set_type(&description, AMXC_VAR_ID_HTABLE);

    if(filter != NULL) {
        retv = amxp_expr_init(&expr, filter);
        when_failed_trace(retv, exit, ERROR, "Failed to init filter, retval = %d", retv);
    }

    amxc_var_for_each(var, ret) {
        if(filter != NULL) {
            amxc_var_t* param = describe_statistic(&description, var);
            if(!amxp_expr_eval_var(&expr, param, &expr_status)) {
                amxc_var_delete(&param);
                continue;
            }
            amxc_var_delete(&param);
        }
        amxc_var_add_key(uint32_t, retval, amxc_var_key(var), GET_UINT32(var, NULL));
        status = amxd_status_ok;
    }

    if(filter != NULL) {
        amxp_expr_clean(&expr);
    }

exit:
    amxc_var_clean(&description);
    return status;
}

amxd_status_t _object_delete(amxd_object_t* object,
                             UNUSED amxd_param_t* param,
                             amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval,
                             UNUSED void* priv) {
    amxd_status_t rv = amxd_status_invalid_action;
    uint32_t instances = amxd_object_get_instance_count(object);

    when_false(reason == action_object_del_inst, exit);

    if(instances > 1) {
        rv = amxd_status_ok;
    }

exit:
    SAH_TRACEZ_INFO(ME, "%d instance(s) of object %s, (reason %d), retval %d",
                    instances, object->name, reason, rv);

    return rv;
}

static void read_stats(amxd_object_t* object, amxc_var_t* ret) {
    amxc_var_t params;
    const char* controller = NULL;
    int retval = -1;

    amxc_var_init(&params);
    when_null_trace(object, exit, ERROR, "object is null");
    when_null_trace(ret, exit, ERROR, "ret is null");

    controller = GET_CHAR(amxd_object_get_param_value(amxd_object_get_parent(object), "Controller"), NULL);
    when_str_empty_trace(controller, exit, ERROR, "No Controller specified");
    retval = amxm_execute_function(controller, controller, "dhcpv4c-get-statistics", &params, ret);
    amxc_var_dump(ret, 0);
    when_failed_trace(retval, exit, ERROR, "Failed to execute dhcpv4c-get-statistics, retval = %d", retval);

exit:
    amxc_var_clean(&params);
    return;
}

static void clear_stats(amxd_object_t* object, amxc_var_t* ret) {
    amxc_var_t params;
    const char* controller = NULL;
    int retval = -1;

    amxc_var_init(&params);
    when_null_trace(object, exit, ERROR, "object is null");
    when_null_trace(ret, exit, ERROR, "ret is null");

    controller = GET_CHAR(amxd_object_get_param_value(amxd_object_get_parent(object), "Controller"), NULL);
    when_str_empty_trace(controller, exit, ERROR, "No Controller specified");
    retval = amxm_execute_function(controller, controller, "dhcpv4c-clear-statistics", &params, ret);
    when_failed_trace(retval, exit, ERROR, "Failed to execute dhcpv4c-clear-statistics, retval = %d", retval);

exit:
    amxc_var_clean(&params);
    return;
}

amxd_status_t _client_object_delete(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    if(object != NULL) {
        client_info_clear((client_info_t*) object->priv);
        object->priv = NULL;
    }
    return amxd_action_object_destroy(object, param, reason, args, retval, priv);
}

amxd_status_t _stats_read(amxd_object_t* object,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_action_object_read(object, param, reason, args, retval, priv);
    amxc_string_t filter;
    amxc_var_t ret;
    amxc_string_init(&filter, 0);
    amxc_var_init(&ret);

    if(status != amxd_status_ok) {
        goto exit;
    }
    read_stats(object, &ret);
    status = amxd_action_object_read_filter(&filter, args);
    if(status == 0) {
        status = get_value_statistics(retval, amxc_string_get(&filter, 0), &ret);
    }

exit:
    amxc_string_clean(&filter);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _stats_list(amxd_object_t* object,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_action_object_list(object, param, reason, args, retval, priv);
    amxc_var_t* params = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);
    if(status != amxd_status_ok) {
        goto exit;
    }
    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    read_stats(object, &ret);
    amxc_var_for_each(var, &ret) {
        amxc_var_add(cstring_t, params, amxc_var_key(var));
    }

exit:
    SAH_TRACEZ_OUT(ME);
    amxc_var_clean(&ret);
    return status;
}

amxd_status_t _stats_describe(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_action_object_describe(object, param, reason, args, retval, priv);
    amxc_var_t* params = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);
    if(status != amxd_status_ok) {
        goto exit;
    }
    read_stats(object, &ret);
    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_for_each(var, &ret) {
        describe_statistic(params, var);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    amxc_var_clean(&ret);
    return status;
}

amxd_status_t _ClearStatistics(amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               UNUSED amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t retval;
    amxc_var_init(&retval);

    clear_stats(object, &retval);

    amxc_var_clean(&retval);
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}

amxd_status_t _return_false(UNUSED amxd_object_t* object,
                            UNUSED amxd_param_t* param,
                            amxd_action_t reason,
                            UNUSED const amxc_var_t* const args,
                            amxc_var_t* const retval,
                            UNUSED void* priv) {
    if(reason != action_param_read) {
        return amxd_status_function_not_implemented;
    }
    amxc_var_set(bool, retval, false);
    return amxd_status_ok;
}

amxd_status_t _renew_lease(amxd_object_t* object,
                           UNUSED amxd_param_t* param,
                           amxd_action_t reason,
                           const amxc_var_t* const args,
                           amxc_var_t* const retval,
                           UNUSED void* priv) {
    int ret = 1;
    amxc_var_t params;
    amxd_status_t status = amxd_status_ok;
    client_info_t* info = NULL;
    bool renew_enabled = false;
    char* ifname = NULL;

    if(reason != action_param_write) {
        return amxd_status_function_not_implemented;
    }

    amxc_var_init(&params);
    amxd_object_get_params(object, &params, amxd_dm_access_public);
    // Check if user set param to true
    renew_enabled = amxc_var_dyncast(bool, args);
    info = (client_info_t*) object->priv;
    when_false(renew_enabled, exit);
    when_null_trace(info, exit, ERROR, "NULL ptr for info_client");
    ifname = amxd_object_get_value(cstring_t, object, "IfName", NULL);
    if(GET_BOOL(&params, "Enable") == false) {
        // Do nothing if DHCPv4 client is disabled
        SAH_TRACEZ_INFO(ME, "func renew for %s disabled", ifname);
    } else {
        char* mod_controller = amxd_object_get_value(cstring_t, object,
                                                     "Controller", NULL);
        ret = dhcpv4c_mod_renew_release(mod_controller, ifname, true);
        SAH_TRACEZ_INFO(ME, "%s renew for %s returned %d",
                        mod_controller, ifname, ret);
        if(ret == 0) {
            amxc_ts_now(&info->lease_timer);
        }
        free(mod_controller);
    }
    free(ifname);
exit:
    amxc_var_set(int32_t, retval, ret);
    amxc_var_clean(&params);
    return status;
}

amxd_status_t _release_lease(amxd_object_t* object,
                             UNUSED amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             UNUSED void* priv) {
    int ret = 1;
    amxc_var_t params;
    amxd_status_t status = amxd_status_ok;
    client_info_t* info = NULL;
    bool release_enabled = false;
    char* ifname = NULL;

    if(reason != action_param_write) {
        return amxd_status_function_not_implemented;
    }

    amxc_var_init(&params);
    amxd_object_get_params(object, &params, amxd_dm_access_public);
    // Check if user set param to true
    release_enabled = amxc_var_dyncast(bool, args);
    info = (client_info_t*) object->priv;
    when_false(release_enabled, exit);
    when_null_trace(info, exit, ERROR, "NULL ptr for info_client");
    ifname = amxd_object_get_value(cstring_t, object, "IfName", NULL);
    if(GET_BOOL(&params, "Enable") == false) {
        // Do nothing if DHCPv4 client is disabled
        SAH_TRACEZ_INFO(ME, "func release for %s disabled", ifname);
    } else {
        char* mod_controller = amxd_object_get_value(cstring_t, object,
                                                     "Controller", NULL);
        ret = dhcpv4c_mod_renew_release(mod_controller, ifname, false);
        SAH_TRACEZ_INFO(ME, "%s release for %s returned %d",
                        mod_controller, ifname, ret);
        if(ret == 0) {
            amxc_ts_now(&info->lease_timer);
        }
        free(mod_controller);
    }
    free(ifname);
exit:
    amxc_var_set(int32_t, retval, ret);
    amxc_var_clean(&params);

    return status;
}

amxd_status_t _read_lease_time_remaining(amxd_object_t* object,
                                         UNUSED amxd_param_t* param,
                                         amxd_action_t reason,
                                         UNUSED const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    client_info_t* info = NULL;
    amxc_ts_t now;
    uint64_t elapsed_time = 0;
    int32_t lease_time_remaining = GET_INT32(&param->value, 0);

    when_true_status(reason != action_param_read, exit,
                     status = amxd_status_function_not_implemented);

    when_true(lease_time_remaining == 0, exit);

    info = (client_info_t*) object->priv;
    if(info == NULL) {
        SAH_TRACEZ_ERROR(ME, "NULL ptr for info_client, object <%s>",
                         amxd_object_get_name(object, AMXD_OBJECT_NAMED));
        status = amxd_status_unknown_error;
        goto exit;
    }

    amxc_ts_now(&now);
    if(amxc_ts_compare(&now, &info->lease_timer) == 1) {
        elapsed_time = now.sec - info->lease_timer.sec;
    }
    lease_time_remaining = lease_time_remaining - elapsed_time;
    status = amxd_status_ok;

exit:
    amxc_var_set(int32_t, retval, lease_time_remaining);
    return status;
}

amxd_status_t _check_tag_not_enabled(amxd_object_t* object,
                                     UNUSED amxd_param_t* param,
                                     amxd_action_t reason,
                                     const amxc_var_t* const args,
                                     UNUSED amxc_var_t* const retval,
                                     UNUSED void* priv) {
    amxd_status_t status = amxd_status_invalid_value;
    amxd_object_t* options = NULL;
    amxd_object_t* same_option = NULL;
    bool enable = false;
    const char* alias = NULL;
    uint32_t tag = 0;

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);

    when_null_trace(args, exit, ERROR, "Cannot get the data of the tag to validate.");
    when_null_trace(object, exit, ERROR, "No reference to the data model object given.");

    enable = amxd_object_get_bool(object, "Enable", NULL);

    when_false_status(enable, exit, status = amxd_status_ok);

    tag = GET_UINT32(args, NULL);
    alias = GET_CHAR(amxd_object_get_param_value(object, "Alias"), NULL);

    when_true_trace(tag == 0, exit, ERROR, "Cannot get Tag for the option.");
    options = amxd_object_get_parent(object);
    when_null_trace(options, exit, ERROR, "Parent object of option not found.");

    if(!str_empty(alias)) {
        same_option = amxd_object_findf(options, ".[Alias != '%s' && Tag == %d && Enable == 1].", alias, tag);
    } else {
        same_option = amxd_object_findf(options, ".[Tag == %d && Enable == 1].", tag);
    }

    when_false_trace(same_option == NULL, exit, WARNING, "Option %s with tag %d is already Enabled", amxd_object_get_name(same_option, AMXD_OBJECT_NAMED), tag);

    status = amxd_status_ok;
exit:
    return status;
}

amxd_status_t _check_tag_already_enabled(amxd_object_t* object,
                                         UNUSED amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         UNUSED amxc_var_t* const retval,
                                         UNUSED void* priv) {
    amxd_status_t status = amxd_status_invalid_value;
    amxd_object_t* options = NULL;
    amxd_object_t* same_option = NULL;
    const char* alias = NULL;
    bool enable = false;
    uint32_t tag = 0;

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);

    when_null_trace(args, exit, ERROR, "Cannot get the data of the enable to validate.");
    when_null_trace(object, exit, ERROR, "No reference to the data model object given.");

    enable = GET_BOOL(args, NULL);
    when_false_status(enable, exit, status = amxd_status_ok);

    tag = amxd_object_get_uint8_t(object, "Tag", NULL);
    alias = GET_CHAR(amxd_object_get_param_value(object, "Alias"), NULL);

    when_true_trace(tag == 0, exit, ERROR, "Cannot get Tag for the option.");
    // default value for tag, check will be done with the Tag
    when_true_status(tag == 1, exit, status = amxd_status_ok);
    options = amxd_object_get_parent(object);
    when_null_trace(options, exit, ERROR, "Parent object of option not found.");

    if(!str_empty(alias)) {
        same_option = amxd_object_findf(options, ".[Alias != '%s' && Tag == %d && Enable == 1].", alias, tag);
    } else {
        same_option = amxd_object_findf(options, ".[Tag == %d && Enable == 1].", tag);
    }

    when_false_trace(same_option == NULL, exit, WARNING, "Option %s with tag %d is already Enabled", amxd_object_get_name(same_option, AMXD_OBJECT_NAMED), tag);

    status = amxd_status_ok;
exit:
    return status;
}