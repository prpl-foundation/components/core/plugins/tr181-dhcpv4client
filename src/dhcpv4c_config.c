/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dhcpv4c_config.h"
#include "client_info.h"
#include "dm_client_netmodel.h"
#include "firewall.h"

amxd_object_t* dhcpv4c_config_options_set_path(amxd_trans_t* trans,
                                               amxd_object_t* interface,
                                               bool req_or_sent) {
    const char* str_type_option = (req_or_sent) ? "ReqOption" : "SentOption";
    amxd_object_t* opts_object = NULL;

    if(interface != NULL) {
        opts_object = amxd_object_findf(interface, ".%s.", str_type_option);
        char* path = amxd_object_get_path(opts_object, AMXD_OBJECT_INDEXED);
        SAH_TRACEZ_INFO(ME, "DM path '%s'", path);
        free(path);
    }
    amxd_trans_select_pathf(trans, ".%s.", str_type_option);

    return opts_object;
}

void dhcpv4c_config_options(amxd_trans_t* trans,
                            amxc_var_t* option,
                            amxd_object_t* interface,
                            bool req_or_sent,
                            bool config_or_update) {
    amxd_object_t* opts_object = dhcpv4c_config_options_set_path(trans, interface,
                                                                 req_or_sent);
    amxc_var_for_each(var, option) {
        amxd_object_t* object_opt = NULL;
        const char* str_value = GET_CHAR(var, "Value");
        uint32_t opts_nr = GET_UINT32(var, "Tag");

        if(opts_object != NULL) {
            object_opt = amxd_object_findf(opts_object, "[Tag==%d]", opts_nr);
        }
        if(object_opt == NULL) {
            if(config_or_update == false) {
                continue;
            }
            // Option 'opts_nr' does not yet exists in datamodel
            amxd_trans_add_inst(trans, 0, NULL);
            amxd_trans_set_value(uint32_t, trans, "Tag", opts_nr);
        } else {
            amxd_trans_select_object(trans, object_opt);
        }
        if(config_or_update == true) {
            amxd_trans_set_value(bool, trans, "Enable", GET_BOOL(var, "Enable"));
        }
        // Update Value in datamodel
        amxd_trans_set_value(cstring_t, trans, "Value", (str_value == NULL) ? "" : str_value);

        amxd_trans_select_pathf(trans, ".^.");

        SAH_TRACEZ_INFO(ME, "Option %d %s", opts_nr,
                        (object_opt == NULL) ? "added" : "changed");
    }

    amxd_trans_select_pathf(trans, ".^.");
}

void dhcpv4c_config_add_options(amxd_object_t* client,
                                amxc_var_t* data,
                                bool req_or_sent) {
    const char* opts = req_or_sent ? "ReqOption" : "SentOption";
    amxc_var_t* var_opts = amxc_var_add_key(amxc_llist_t, data, opts, NULL);
    amxc_llist_t options;
    amxc_llist_init(&options);

    amxd_object_resolve_pathf(client, &options, ".%s.*", opts);
    amxc_llist_for_each(it, (&options)) {
        const char* path = amxc_string_get(amxc_string_from_llist_it(it), 0);
        amxd_object_t* obj = amxd_dm_findf(dhcpv4client_get_dm(), "%s", path);
        uint8_t tag = amxd_object_get_value(uint8_t, obj, "Tag", NULL);
        bool enabled = amxd_object_get_value(bool, obj, "Enable", NULL);
        amxc_var_t* option = amxc_var_add(amxc_htable_t, var_opts, NULL);
        amxc_var_add_key(uint8_t, option, "Tag", tag);
        amxc_var_add_key(bool, option, "Enable", enabled);
        if(req_or_sent == false) {
            // Only when SentOption
            char* value = amxd_object_get_value(cstring_t, obj, "Value", NULL);
            amxc_var_add_key(cstring_t, option, "Value", value);
            free(value);
        }
    }
    amxc_llist_clean(&options, amxc_string_list_it_free);
}

int dhcpv4c_config_client_update(client_info_t* info, bool enable) {
    char* mod_name = NULL;
    int retval = 1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_null_trace(info, exit, ERROR, "ptr to client_info structure is null");
    when_null_trace(info->obj, exit, ERROR, "Object for interface %s is null", info->intf_path);
    mod_name = amxd_object_get_value(cstring_t, info->obj, "Controller", NULL);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &data, "Enable", enable);
    amxc_var_add_key(cstring_t, &data, "Interface", info->intf_path);
    dhcpv4c_config_add_options(info->obj, &data, true);
    dhcpv4c_config_add_options(info->obj, &data, false);

    retval = amxm_execute_function((const char*) mod_name, (const char*) mod_name,
                                   "client-change", &data, &ret);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Mod %s, func 'client-change' returned %d",
                         mod_name, retval);
        goto exit;
    }
    retval = GET_INT32(&ret, NULL);
    SAH_TRACEZ_INFO(ME, "module func 'client-change' returned %d", retval);

    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    retval = amxm_execute_function((const char*) mod_name, (const char*) mod_name,
                                   "update-config", &data, &ret);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Mod %s, func 'update-config' returned %d",
                         mod_name, retval);
        goto exit;
    }
    retval = GET_INT32(&ret, NULL);
    SAH_TRACEZ_INFO(ME, "module func 'update-config' returned %d", retval);
exit:
    free(mod_name);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);
    return retval;
}
