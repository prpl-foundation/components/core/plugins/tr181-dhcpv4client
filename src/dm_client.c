/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/client.h>

#include "dhcpv4client.h"
#include "dm_client.h"
#include "client_info.h"

static char* dm_client_clientid_use_mac_addr(amxd_object_t* client) {
    amxc_string_t str_client;
    client_info_t* info = (client_info_t*) client->priv;
    amxc_var_t* data = netmodel_getFirstParameter(info->intf_path, "MACAddress", "", "down");
    const char* mac_addr = amxc_var_constcast(cstring_t, data);
    char* clientid = NULL;
    SAH_TRACEZ_INFO(ME, "MACAddress for %s is %s", info->intf_path, mac_addr);
    when_str_empty_trace(mac_addr, exit, ERROR, "Unable to get MAC address for path %s",
                         info->intf_path);
    amxc_string_init(&str_client, 0);
    amxc_string_setf(&str_client, "01%s", mac_addr);
    amxc_string_replace(&str_client, ":", "", UINT32_MAX);
    clientid = amxc_string_take_buffer(&str_client);
    amxc_string_clean(&str_client);
exit:
    amxc_var_delete(&data);
    return clientid;
}

void dm_client_clientid_init(amxd_object_t* client) {
    amxd_object_t* opts_object = NULL;
    char* incl_clientid = NULL;
    char* clientid = NULL;
    when_null_trace(client, exit, ERROR, "client object is null");
    incl_clientid = amxd_object_get_value(cstring_t, client, "IncludeClientId", NULL);
    when_str_empty(incl_clientid, exit);
    opts_object = amxd_object_findf(client, ".SentOption.[Tag==61].");
    when_null_trace(opts_object, exit, INFO, "SentOption 61 is not specified");
    if(strcmp(incl_clientid, "MACAddress") == 0) {
        clientid = dm_client_clientid_use_mac_addr(client);
    }/* else if(strcmp(incl_clientid, ... for future keywords */

    if(clientid != NULL) {
        amxd_trans_t trans;
        amxd_trans_init(&trans);
        amxd_trans_select_object(&trans, opts_object);
        amxd_trans_set_value(cstring_t, &trans, "Value", clientid);
        amxd_trans_apply(&trans, dhcpv4client_get_dm());
        amxd_trans_clean(&trans);
    }
exit:
    free(incl_clientid);
    free(clientid);
}
