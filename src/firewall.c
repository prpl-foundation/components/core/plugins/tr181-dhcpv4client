/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "firewall.h"
#include <amxm/amxm.h>
// sahtrace
#define ME "firewall"

static amxc_var_t* services = NULL;  // database for key value pairs: interface (key) / Firewall.X_Prpl_Service.Alias

static int firewall_add_service(const char* alias,
                                const char* interface) {
    int retval = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* tmp = NULL;
    amxc_string_t interface_path;

    amxc_string_init(&interface_path, 0);
    amxc_string_set(&interface_path, interface);
    if(amxc_string_search(&interface_path, "Device.", 0) != 0) {
        amxc_string_prepend(&interface_path, "Device.", 7);
    }

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", alias);
    tmp = amxc_var_add_new_key(&args, "interface");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&interface_path));
    amxc_var_add_key(cstring_t, &args, "ipversion", "4");
    amxc_var_add_key(cstring_t, &args, "protocol", "17"); // Only UDP
    amxc_var_add_key(uint32_t, &args, "destination_port", DHCP_CLIENT_PORT);
    amxc_var_add_key(bool, &args, "enable", true);

    if(0 != amxm_execute_function("fw", "fw", "set_service", &args, &ret)) {
        SAH_TRACEZ_ERROR(ME, "Failed to open firewall port on interface[%s]",
                         interface);
        retval = -1;
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&interface_path);
    return retval;
}

int firewall_open_dhcp_port(const char* interface,
                            const char* name) {
    int retval = -1;
    int intf_nr = 0;
    amxc_string_t alias_str;
    const char* alias = NULL;

    amxc_string_init(&alias_str, 0);
    when_str_empty(interface, exit);
    sscanf(interface, "IP.Interface.%d.", &intf_nr);

    SAH_TRACEZ_INFO(ME, "opening firewall port %d on interface[%s]",
                    DHCP_CLIENT_PORT, interface);

    if(services == NULL) {
        if(amxc_var_new(&services) != 0) {
            SAH_TRACEZ_WARNING(ME, "failed to create database");
            goto exit;
        }
        amxc_var_set_type(services, AMXC_VAR_ID_HTABLE);
    } else if(amxc_var_get_key(services, interface, AMXC_VAR_FLAG_DEFAULT) != NULL) {
        SAH_TRACEZ_WARNING(ME, "found existing firewall service with interface[%s]",
                           interface);
        retval = 0;
        goto exit;
    }

    amxc_string_appendf(&alias_str, FW_ALIAS_PREFIX "%s", name);
    alias = amxc_string_get(&alias_str, 0);

    retval = firewall_add_service(alias, interface);
    if(retval == 0) {
        amxc_var_add_key(cstring_t, services, interface, alias);
        SAH_TRACEZ_INFO(ME, "added firewall service[%s]", alias);
    }
exit:
    amxc_string_clean(&alias_str);
    return retval;
}

int firewall_close_dhcp_port(const char* interface,
                             const char* name) {
    int retval = -1;
    SAH_TRACEZ_INFO(ME, "closing firewall port %d for %s", DHCP_CLIENT_PORT,
                    (interface == NULL) ? "all interfaces" : interface);
    amxc_var_for_each(var, services) {
        amxc_var_t args;
        amxc_var_t ret;
        const char* alias = amxc_var_constcast(cstring_t, var);
        if((interface != NULL) && (name != NULL) &&
           (strcmp(&alias[FW_ALIAS_PREFIX_LEN], name) != 0)) {
            // do not delete this one
            continue;
        }

        // delete service if interface name is found or if interface == NULL
        amxc_var_init(&args);
        amxc_var_init(&ret);

        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "id", alias);
        if(0 != amxm_execute_function("fw", "fw", "delete_service", &args, &ret)) {
            SAH_TRACEZ_ERROR(ME, "failed to remove firewall service[%s]", alias);
            goto loop;
        }
        retval = 0;
        SAH_TRACEZ_INFO(ME, "removed firewall service[%s]", alias);
        amxc_var_take_it(var);
        amxc_var_delete(&var);
loop:
        amxc_var_clean(&args);
        amxc_var_clean(&ret);
    }
    return retval;
}

void firewall_cleanup(void) {
    firewall_close_dhcp_port(NULL, NULL);

    // all key value pairs have been handled, destroy the database
    amxc_var_delete(&services);
    services = NULL;
}
