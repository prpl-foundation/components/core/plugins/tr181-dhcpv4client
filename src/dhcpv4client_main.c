/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "dm_dhcpv4client.h"
#include "dm_client_netmodel.h"
#include "dhcpv4c_config.h"
#include "firewall.h"

typedef struct _dhcpv4_client {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    int timeout;
    amxc_var_t active_fds;
    amxm_shared_object_t* so;
} _dhcpv4_client_t;

static _dhcpv4_client_t app;

int dhcpv4client_get_timeout(void) {
    return app.timeout;
}

amxd_dm_t* dhcpv4client_get_dm(void) {
    return app.dm;
}

amxc_var_t* dhcpv4client_get_config(void) {
    return &app.parser->config;
}

amxo_parser_t* dhcpv4client_get_parser(void) {
    return app.parser;
}

static int dhcpv4c_load_controllers(void) {
    int retval = 1;
    amxd_object_t* dhcpv4_obj = amxd_dm_findf(dhcpv4client_get_dm(), "DHCPv4Client");
    const char* module_dir = GET_CHAR(dhcpv4client_get_config(), "module-client-dir");
    const amxc_var_t* controllers =
        amxd_object_get_param_value(dhcpv4_obj, "ClientSupportedControllers");
    amxc_var_t lcontrollers;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);
    when_str_empty_trace(module_dir, exit, ERROR, "Missing or empty " \
                         "parameter 'module-client-dir'");
    retval = amxd_status_parameter_not_found;
    when_null_trace(controllers, exit, ERROR, "Missing parameter 'ClientSupportedControllers'");
    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "%s/%s.so", module_dir, name);
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)",
                        name, amxc_string_get(&mod_path, 0));
        retval = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        when_failed_trace(retval, exit, WARNING,
                          "Loading controller '%s' failed", name);
    }
    retval = 0;
exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    return retval;
}

static int load_module(const char* dir, const char* module, const char* controller) {
    const char* path = NULL;
    amxc_string_t path_str;
    int rv = 2;

    amxc_string_init(&path_str, 0);
    when_str_empty_trace(module, exit, ERROR, "controller string is empty");

    amxc_string_setf(&path_str, "%s/%s.so", dir, module);
    path = amxc_string_get(&path_str, 0);

    SAH_TRACEZ_NOTICE(ME, "Load module[%s] for %s", path, controller);

    rv = amxm_so_open(&app.so, controller, path);
    when_failed_trace(rv, exit, WARNING,
                      "Loading controller '%s' failed", controller);

exit:
    amxc_string_clean(&path_str);
    return rv;
}

static int dhcpv4c_load_fw_controllers(void) {
    int retval = 1;
    amxd_object_t* dhcpv4_obj = amxd_dm_findf(dhcpv4client_get_dm(), "DHCPv4Client");
    const char* module_dir = GET_CHAR(dhcpv4client_get_config(), "fw-directory");
    const amxc_var_t* controllers =
        amxd_object_get_param_value(dhcpv4_obj, "SupportedFWControllers");
    amxc_var_t lcontrollers;
    amxc_string_t mod_path;

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);
    when_str_empty_trace(module_dir, exit, ERROR, "Missing or empty " \
                         "parameter 'module-client-dir'");
    retval = amxd_status_parameter_not_found;
    when_null_trace(controllers, exit, ERROR, "Missing parameter 'SupportedFWControllers'");
    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        when_null_trace(name, exit, ERROR, "Can't get controller name");
        amxc_string_setf(&mod_path, "%s/%s.so", module_dir, name);
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)",
                        name, amxc_string_get(&mod_path, 0));
        retval = load_module(module_dir, name, "fw");
        when_failed_trace(retval, exit, WARNING,
                          "Loading controller '%s' failed", name);
    }
    retval = 0;
exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    return retval;
}
static void dhcpv4c_read_handler(UNUSED int fd, void* data) {
    amxc_var_t* var = (amxc_var_t*) data;
    amxc_var_t ret;
    const char* function = GET_CHAR(var, "handler");
    const char* mod_name = "mod-dhcpv4c";

    amxc_var_init(&ret);
    when_str_empty_trace(function, exit, ERROR, "Module function missing");

    amxm_execute_function(mod_name, mod_name, function, var, &ret);

exit:
    amxc_var_clean(&ret);
    return;
}

static int dhcpv4c_add_fd(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    int rv = -1;
    const char* intf = GET_CHAR(args, "intf_name");
    int fd = amxc_var_constcast(fd_t, GET_ARG(args, "fd"));
    amxc_var_t* var = NULL;

    when_str_empty_trace(intf, exit, ERROR, "No valid interface name given");

    var = GET_ARG(&app.active_fds, intf);
    when_not_null_trace(var, exit, ERROR, "Already have a listen connection for interface %s", intf);

    var = amxc_var_add_new_key(&app.active_fds, intf);
    amxc_var_copy(var, args);
    rv = amxo_connection_add(dhcpv4client_get_parser(), fd, dhcpv4c_read_handler, NULL, AMXO_LISTEN, var);
    when_failed_trace(rv, exit, ERROR, "Could not add connection for intf %s and fd %d", intf, fd);

exit:
    return rv;
}

static int dhcpv4c_rm_fd(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    int rv = -1;
    const char* intf = GET_CHAR(args, "intf_name");
    int fd = amxc_var_constcast(fd_t, GET_ARG(args, "fd"));
    amxc_var_t* var = NULL;

    when_str_empty_trace(intf, exit, ERROR, "No valid interface name given");

    var = amxc_var_take_key(&app.active_fds, intf);
    when_null_trace(var, exit, ERROR, "No connection found for interface %s", intf);

    rv = amxo_connection_remove(dhcpv4client_get_parser(), fd);
    when_failed_trace(rv, exit, ERROR, "Could not remove connection for intf %s and fd %d", intf, fd);
    amxc_var_delete(&var);

exit:
    return rv;
}

/**
   @brief
   Register function(s) for use in modules specified by keywords
   'use-dhcpv4c' and/or 'use-mod-config'

   @return
   0 success.
   3 failure
 */
static int dhcpv4c_register_func(void) {
    int retval = 3;
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    when_null(so, exit);

    when_failed(amxm_module_register(&mod, so, MOD_CORE), exit);

    retval = amxm_module_add_function(mod, "update-client", dm_update_client);
    retval |= amxm_module_add_function(mod, "client-misconfigured", dm_client_misconfigured);
    retval |= amxm_module_add_function(mod, "event-loop-add-fd", dhcpv4c_add_fd);
    retval |= amxm_module_add_function(mod, "event-loop-rm-fd", dhcpv4c_rm_fd);
    SAH_TRACEZ_INFO(ME, "Registering functions returned %d", retval);
    retval = (retval != 0) ? 3 : 0;
exit:
    return retval;
}

/**
   @brief
   Load a module that is responsible to handles the DHCPv4 client
   functionality, either directly (the module itself) or indirectly
   by calling for example udhcpc.

   @param
   dm a pointer to the datamodel of this plugin
   parser pointer to the parser.

   @return
   0 success.
   1 parameter 'module-client-dir' not found in config section
   2 failed to load module specified by datamodel parameter 'ClientSupportedControllers'
   3 registering function(s) failed
   4 Parameter 'ClientSupportedControllers' not found in datamodel
 */
static int dhcpv4c_init(amxd_dm_t* dm,
                        amxo_parser_t* parser) {
    int retval = 0;

    app.dm = dm;
    app.parser = parser;
    app.timeout = 5;

    amxc_var_init(&app.active_fds);
    amxc_var_set_type(&app.active_fds, AMXC_VAR_ID_HTABLE);

    dm_storage_init();
    retval = dhcpv4c_register_func();
    when_failed_trace(retval, exit, ERROR, "failed to register " \
                      "exported functions (%d)", retval);
    retval = dhcpv4c_load_controllers();
    when_failed_trace(retval, exit, ERROR, "failed to load 1 or " \
                      "more controllers (%d)", retval);
    retval = dhcpv4c_load_fw_controllers();
    when_failed_trace(retval, exit, ERROR, "failed to load 1 or " \
                      "more controllers (%d)", retval);
    client_netmodel_init();
exit:
    return retval;
}

static void dhcpv4c_clean (void) {
    amxc_var_clean(&app.active_fds);
    dm_storage_clean();
    firewall_cleanup();
    client_netmodel_cleanup();
    app.dm = NULL;
    app.parser = NULL;
    amxm_close_all();
}

int _dhcpv4client_main(int reason,
                       amxd_dm_t* dm,
                       amxo_parser_t* parser) {
    int retval = 0;

    SAH_TRACEZ_INFO(ME, "dhcpv4client_main, reason: %d", reason);
    switch(reason) {
    case 0:
        // START
        retval = dhcpv4c_init(dm, parser);
        break;
    case 1:
        // STOP
        dhcpv4c_clean();
        break;
    }

    return retval;
}
