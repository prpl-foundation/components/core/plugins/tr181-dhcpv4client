/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "dm_dhcpv4client.h"
#include "client_info.h"
#include "dhcpv4c_config.h"
#include "dhcpv4c_delay_update.h"
#include "firewall.h"

void _print_event(const char* const event_name,
                  const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    (void) event_name; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Event received %s%s", event_name,
                    (event_data != NULL) ? ", event data:" : "");
    if(event_data != NULL) {
        amxc_var_log(event_data);
    }
}

void _dhcpv4client_instance_added(UNUSED const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  UNUSED void* const priv) {
    amxd_object_t* client = NULL;
    amxd_object_t* client_templ = amxd_dm_signal_get_object(dhcpv4client_get_dm(), event_data);
    when_null_trace(client_templ, exit, ERROR, "could not get client template object");

    client = amxd_object_get_instance(client_templ, NULL, GET_UINT32(event_data, "index"));
    when_null_trace(client, exit, ERROR, "could not get client with index %d",
                    GET_UINT32(event_data, "index"));
    client_info_create(client, true);
exit:
    return;
}

void _dhcpv4client_enable_changed(UNUSED const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  UNUSED void* const priv) {
    int retval = 0;
    const char* datapath = NULL;
    char* interface = NULL;
    amxd_object_t* dm_client = NULL;
    bool client_is_enabled = GETP_BOOL(event_data, "parameters.Enable.to");

    datapath = GETP_CHAR(event_data, "path");
    SAH_TRACEZ_INFO(ME, "<%s> enabled: %s", datapath,
                    (client_is_enabled ? "true" : "false"));

    dm_client = amxd_dm_findf(dhcpv4client_get_dm(), "%s", datapath);
    SAH_TRACEZ_INFO(ME, "select object %s", (dm_client == NULL) ? "not ok" : "ok");
    when_null_trace(dm_client, exit, ERROR, "Datapath %s, object is null", datapath);
    client_info_update(dm_client, client_is_enabled);
    interface = amxd_object_get_value(cstring_t, dm_client, "Interface", NULL);
    // Firewall; Enable = 1 enable port before receiving DHCP messages
    //           Enable = 0 disable when function dm_update_client is called with bound = false
    if(client_is_enabled) {
        char* alias = amxd_object_get_value(cstring_t, dm_client, "Alias", NULL);
        firewall_open_dhcp_port(interface, alias);
        free(alias);
    }
    dm_dhcpv4client_init_client(dm_client,
                                client_is_enabled ? dhcpv4c_status_enabled : dhcpv4c_status_disabled,
                                dhcpv4c_dhcp_status_init);

    retval = dhcpv4c_config_client_update((client_info_t*) dm_client->priv, client_is_enabled);
    (void) retval; // use variable when tracing is disabled.
    SAH_TRACEZ_INFO(ME, "Update configuration for %s returned %d",
                    interface, retval);
exit:
    free(interface);
}

void _dhcpv4c_option_changed(UNUSED const char* const event_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    const char* datapath = GETP_CHAR(event_data, "path");
    dm_update_client_config(datapath);
}

void _dhcpv4c_object_changed(UNUSED const char* const event_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    int retval = -1;
    amxd_object_t* client = amxd_dm_signal_get_object(dhcpv4client_get_dm(), event_data);
    client_info_t* info = NULL;
    when_null_trace(client, exit, ERROR, "object is null");
    info = (client_info_t*) client->priv;
    SAH_TRACEZ_INFO(ME, "<%s> changed", GETP_CHAR(event_data, "path"));
    dm_dhcpv4client_init_client(client, dhcpv4c_status_disabled,
                                dhcpv4c_dhcp_status_init);
    if(info != NULL) {
        // Disable dhcp for previous interface in configuration
        retval = dhcpv4c_config_client_update(info, false);
        (void) retval; // use variable when tracing is disabled
        SAH_TRACEZ_INFO(ME, "Update configuration for %s returned %d",
                        info->intf_path, retval);
        client_info_clear(info);
        client->priv = NULL;
    } else {
        SAH_TRACEZ_INFO(ME, "client info was null");
    }
    client_info_create(client, true);
exit:
    return;
}

void _dhcpv4c_timer_changed(UNUSED const char* const event_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    amxd_object_t* client = amxd_dm_signal_get_object(dhcpv4client_get_dm(), event_data);
    int32_t time = GET_UINT32(event_data, "parameters.ResetOnPhysDownTimeout.to");
    client_info_t* info = NULL;
    when_null_trace(client, exit, ERROR, "object is null");
    info = (client_info_t*) client->priv;

    SAH_TRACEZ_INFO(ME, "time <%d> changed", time);

    //timer is started need to restart it with good timing
    if((amxp_timer_get_state(info->timer_physdown) == amxp_timer_started) || (amxp_timer_get_state(info->timer_physdown) == amxp_timer_running)) {
        amxp_timer_stop(info->timer_physdown);
        if(time > 0) {
            SAH_TRACEZ_INFO(ME, "Timer started");
            amxp_timer_start(info->timer_physdown, time);
        }
        // directly shutdown if timer == 0
        if(time == 0) {
            SAH_TRACEZ_INFO(ME, "Directly stopped");
            dm_dhcpv4client_start_stop_client(info, false);
        }
        // otherwise, never shutdown
    }
exit:
    return;
}

void _dhcpv4c_config_changed(UNUSED const char* const event_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    amxc_var_t params;
    amxc_var_t ret;
    const char* ifname = NULL;
    const char* controller = NULL;
    int retval = -1;
    amxd_object_t* client = amxd_dm_signal_get_object(dhcpv4client_get_dm(), event_data);

    amxc_var_init(&params);
    amxc_var_init(&ret);
    when_null_trace(client, exit, ERROR, "object is null");

    amxd_object_get_params(client, &params, amxd_dm_access_private);
    controller = GET_CHAR(&params, "Controller");
    when_str_empty_trace(controller, exit, ERROR, "No Controller specified");

    // Get the interface name (IfName)
    ifname = GET_CHAR(&params, "IfName");
    when_str_empty_trace(ifname, exit, WARNING,
                         "No IfName set, not updating parameters");
    // If no IfName is found, do nothing, the info will be sent when the client starts

    // Use the params var as argument for the amxm call,
    // all necessary data is present in here so there is no need to copy it.
    retval = amxm_execute_function(controller, controller, "dhcpv4c-config-update", &params, &ret);

    when_failed_trace(retval, exit, ERROR, "Failed to execute dhcpv4c-config-update, retval = %d", retval);

exit:
    amxc_var_clean(&params);
    amxc_var_clean(&ret);
    return;
}
