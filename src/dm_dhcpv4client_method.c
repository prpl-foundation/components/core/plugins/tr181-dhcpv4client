/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "dm_dhcpv4client.h"

static const char* dhcpv4client_error_to_status(const char* error) {
    const char* str = "Error";

    when_str_empty_trace(error, exit, ERROR, "Error string is empty");

    if(strcmp(error, "None") == 0) {
        str = "Enabled";
    } else if(strcmp(error, "LocalRelease") == 0) {
        str = "Disabled";
    } else if((strcmp(error, "RemoteNack") == 0) ||
              (strcmp(error, "Authentication Failure") == 0)) {
        str = "Error_Misconfigured";
    }

exit:
    return str;
}

int dhcpv4c_mod_renew_release(const char* mod_name,
                              const char* intf_name,
                              bool renew) {
    const char* func = (renew) ? "dhcpv4c-renew" : "dhcpv4c-release";
    int retval = 1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_null(mod_name, exit);
    when_null(intf_name, exit);

    amxc_var_set(cstring_t, &data, intf_name);

    retval = amxm_execute_function(mod_name, mod_name, func, &data, &ret);
    if(retval != 0) {
        // Function should be implemented
        SAH_TRACEZ_ERROR(ME, "Mod %s, func '%s' returned %d",
                         mod_name, func, retval);
        goto exit;
    }
    retval = GET_INT32(&ret, NULL);
    SAH_TRACEZ_INFO(ME, "module func '%s' returned %d", func, retval);
exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    return retval;
}

amxd_status_t _update(UNUSED amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    amxc_var_t* data = NULL;
    const char* mod_name = NULL;
    const char* func_name = NULL;
    int retval = 1;

    mod_name = GET_CHAR(args, "mod_name");
    func_name = GET_CHAR(args, "fnc");
    data = amxc_var_get_path(args, "data", AMXC_VAR_FLAG_DEFAULT);

    SAH_TRACEZ_INFO(ME, "Mod %s, func '%s', data %sok", mod_name, func_name,
                    (data == NULL) ? "not " : "");
    when_null(mod_name, exit);
    when_null(func_name, exit);
    when_null(data, exit);

    retval = amxm_execute_function(mod_name, mod_name, func_name, data, ret);
    if(retval != 0) {
        // Function should be implemented
        SAH_TRACEZ_ERROR(ME, "Mod %s, func '%s' returned %d",
                         mod_name, func_name, retval);
        amxc_var_set(cstring_t, ret, "error_invalid_function");
        goto exit;
    }
exit:
    return amxd_status_ok;
}

/*
   Arguments must at least contain a htable variant named "parameters"

   example
   arg = {
      parameters = {
          ifname = "eth0",
          bound = true
      }
   }
 */
int dm_update_client(const char* function_name,
                     amxc_var_t* args,
                     amxc_var_t* ret) {
    int retval = -1;
    const char* ifname = NULL;
    amxd_object_t* dm_dhcp_client = NULL;
    amxc_var_t* parameters = NULL;
    const char* status = NULL;
    amxd_trans_t trans;

    (void) function_name; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Function called - %s", function_name);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    parameters = GET_ARG(args, "parameters");
    when_null_trace(parameters, exit, ERROR, "no parameters");
    when_false_trace((parameters->type_id == AMXC_VAR_ID_HTABLE), exit,
                     ERROR, "Incorrect type, parameters");
    ifname = GET_CHAR(parameters, "ifname");
    when_str_empty_trace(ifname, exit, ERROR, "no ifname given");

    dm_dhcp_client = amxd_dm_findf(dhcpv4client_get_dm(),
                                   "DHCPv4Client.Client.[IfName=='%s']", ifname);
    // If not found, store data in htable for later
    if(dm_dhcp_client == NULL) {
        SAH_TRACEZ_INFO(ME, "Received dhcp info for intf %s,"
                        "dm does not know this intf yet, storing data", ifname);
        dm_storage_store_data(ifname, args);
        goto exit;
    }
    amxd_trans_select_object(&trans, dm_dhcp_client);

    // If "status" is present, this info is coming from the mod-dhcpv4c dhcp client
    status = GET_CHAR(parameters, "status");
    if(status != NULL) {
        // Treat "Idle" as if it was "Init"
        // TR181 does not know Idle but we still need to clean up the dm
        if(strcmp(status, "Idle") == 0) {
            status = "Init";
        }

        // Check if status is among the supported tr181 statuses, if not, ignore this update
        when_false_status(dm_dhcpv4client_is_status_supported(status), exit, retval = 0);

        // Always update the specific error info
        amxd_trans_set_param(&trans, "LastConnectionError", GET_ARG(parameters, "error"));

        if(strcmp(status, "Bound") == 0) {
            dm_update_client_bound(dm_dhcp_client, parameters);
        } else if(strcmp(status, "Init") == 0) {
            dm_update_client_deconfig(dm_dhcp_client);
        } else {
            amxd_trans_set_value(cstring_t, &trans, "DHCPStatus", status);
            amxd_trans_set_value(cstring_t, &trans, "Status",
                                 dhcpv4client_error_to_status(GET_CHAR(parameters, "error")));
        }
        amxd_trans_apply(&trans, dhcpv4client_get_dm());
    } else {
        // This info is coming from udhcpc
        if(GET_BOOL(parameters, "bound") == false) {
            dm_update_client_deconfig(dm_dhcp_client);
        } else {
            dm_update_client_bound(dm_dhcp_client, parameters);
        }
    }
    retval = 0;

exit:
    amxd_trans_clean(&trans);
    amxc_var_set(int32_t, ret, retval);
    return retval;
}

int dm_client_misconfigured(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    int retval = -1;
    amxd_object_t* dm_client = NULL;
    const char* interface = GET_CHAR(args, "Interface");
    when_str_empty_trace(interface, exit, ERROR, "no interface path given");

    dm_client = amxd_dm_findf(dhcpv4client_get_dm(),
                              "DHCPv4Client.Client.[Interface=='%s']", interface);
    when_null_trace(dm_client, exit, ERROR, "no dm_client found");
    dm_dhcpv4client_init_client(dm_client, dhcpv4c_status_error_misconfigure,
                                dhcpv4c_dhcp_status_init);
    retval = 0;
exit:
    amxc_var_set(int32_t, ret, retval);
    return retval;
}

amxd_status_t _Renew(amxd_object_t* client_object,
                     UNUSED amxd_function_t* func,
                     UNUSED amxc_var_t* args,
                     UNUSED amxc_var_t* ret) {
    char* mod_controller = NULL;
    char* ifname = NULL;
    bool enable = false;
    client_info_t* info = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(client_object, exit, ERROR, "No client object given");
    when_null_trace(client_object->priv, exit, ERROR, "No private data for client found");

    info = (client_info_t*) client_object->priv;

    mod_controller = amxd_object_get_value(cstring_t, client_object, "Controller", NULL);
    when_null_trace(mod_controller, exit, ERROR, "No back-end module found for the client");

    ifname = amxd_object_get_value(cstring_t, client_object, "IfName", NULL);
    when_null_trace(mod_controller, exit, ERROR, "No IfName found for the client");
    enable = amxd_object_get_value(bool, client_object, "Enable", NULL);

    if(!enable) {
        SAH_TRACEZ_INFO(ME, "Renew disabled");
    } else {
        status = dhcpv4c_mod_renew_release(mod_controller, ifname, true);
        if(status == 0) {
            amxc_ts_now(&info->lease_timer);
        }
    }
exit:
    free(mod_controller);
    free(ifname);
    return status;
}

amxd_status_t _Release(amxd_object_t* client_object,
                       UNUSED amxd_function_t* func,
                       UNUSED amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    char* mod_controller = NULL;
    char* ifname = NULL;
    bool enable = false;
    client_info_t* info = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(client_object, exit, ERROR, "No client object given");
    when_null_trace(client_object->priv, exit, ERROR, "No private data for client found");

    info = (client_info_t*) client_object->priv;

    mod_controller = amxd_object_get_value(cstring_t, client_object, "Controller", NULL);
    when_null_trace(mod_controller, exit, ERROR, "No back-end module found for the client");

    ifname = amxd_object_get_value(cstring_t, client_object, "IfName", NULL);
    when_null_trace(mod_controller, exit, ERROR, "No IfName found for the client");

    enable = amxd_object_get_value(bool, client_object, "Enable", NULL);

    if(!enable) {
        SAH_TRACEZ_INFO(ME, "Release disabled");
    } else {
        status = dhcpv4c_mod_renew_release(mod_controller, ifname, false);
        if(status == 0) {
            amxc_ts_now(&info->lease_timer);
        }
    }
exit:
    free(mod_controller);
    free(ifname);
    return status;

}
