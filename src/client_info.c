/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "client_info.h"
#include "dm_client_netmodel.h"
#include "dm_dhcpv4client.h"
#include "dhcpv4c_delay_update.h"
#include "dm_client.h"

#define ME "dhcpv4c"

static void client_intf_update_ifname(client_info_t* info) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_object(&trans, info->obj);
    amxd_trans_set_value(cstring_t, &trans, "IfName", info->intf_name);
    SAH_TRACEZ_INFO(ME, "Set IfName transaction returned %d",
                    amxd_trans_apply(&trans, dhcpv4client_get_dm()));
    amxd_trans_clean(&trans);
}

static void client_intf_update_on_ipup(client_info_t* info) {
    amxc_var_t* stored_data = NULL;
    amxc_var_t ret;
    int retval = -1;
    amxc_var_init(&ret);
    // Update ifname in dm
    client_intf_update_ifname(info);
    dm_client_clientid_init(info->obj);
    dm_dhcpv4client_start(info);

    if(info->update) {
        info->update = false;
        SAH_TRACEZ_INFO(ME, "Update configuration");
        dhcpv4c_delay_update(info->update_timer);
    }
    // At boot time, interface name query comes later then data from udhcpc component
    // the data from udhcpc was stored temporarily and processed now
    stored_data = dm_storage_get_data(info->intf_name);
    when_null(stored_data, exit);

    SAH_TRACEZ_INFO(ME, "Found stored dhcp data for intf %s, updating dm", info->intf_name);
    retval = amxm_execute_function("self", MOD_CORE, "update-client", stored_data, &ret);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Mod %s, func 'update-client' returned %d",
                           MOD_CORE, retval);
    }
    amxc_var_delete(&stored_data);
exit:
    amxc_var_clean(&ret);
    return;
}

static void client_isup_netmodel_cb(UNUSED const char* sig_name,
                                    const amxc_var_t* data,
                                    void* priv) {
    client_info_t* info = (client_info_t*) priv;
    const char* intf_name = NULL;

    when_null(data, exit);
    when_null(info, exit);
    when_null(info->obj, exit);
    intf_name = amxc_var_constcast(cstring_t, data);
    SAH_TRACEZ_INFO(ME, "intf_path %s: intf_name %s -> %s",
                    info->intf_path, info->intf_name, intf_name);

    when_null_trace(info->intf_name, update, INFO, "No interface previously set so backend not stopped");

    // If interface is not up, start the timer to stop the backend
    if((intf_name == NULL ) || (intf_name[0] == '\0')) {
        int32_t time = amxd_object_get_value(int32_t, info->obj, "ResetOnPhysDownTimeout", NULL);
        SAH_TRACEZ_INFO(ME, "%s stopping client on intf %s", info->intf_path, info->intf_name);
        amxp_timer_stop(info->timer_physdown);

        if(time > 0) {
            SAH_TRACEZ_INFO(ME, "Timer started");
            amxp_timer_start(info->timer_physdown, time);
        }
        // directly shutdown if timer == 0
        if(time == 0) {
            SAH_TRACEZ_INFO(ME, "Directly stopped");
            dm_dhcpv4client_start_stop_client(info, false);
        }
        // otherwise, never shutdown
    } else {
        // If interface name have changed stop backend
        when_false_trace(strcmp(info->intf_name, intf_name) != 0, exit, INFO, "No change in interface name");
        SAH_TRACEZ_INFO(ME, "Directly stopped to be restarted");
        dm_dhcpv4client_start_stop_client(info, false);
    }
    free(info->intf_name);
    info->intf_name = NULL;

update:
    if((intf_name != NULL) && (intf_name[0] != '\0')) {
        amxp_timer_stop(info->timer_physdown);
        SAH_TRACEZ_INFO(ME, "Timer stopped");

        info->intf_name = strdup(intf_name);
        client_intf_update_on_ipup(info);
    }

exit:
    return;
}

static void client_intf_netmodel_cb(UNUSED const char* sig_name,
                                    const amxc_var_t* data,
                                    void* priv) {
    client_info_t* info = (client_info_t*) priv;
    const char* intf_name = NULL;

    when_null(data, exit);
    when_null(info, exit);
    when_null(info->obj, exit);
    intf_name = amxc_var_constcast(cstring_t, data);
    SAH_TRACEZ_INFO(ME, "intf_path %s: intf_name %s -> %s",
                    info->intf_path, info->intf_name, intf_name);

    if(intf_name == NULL) {
        // Stop backend if the interface is removed
        SAH_TRACEZ_INFO(ME, "%s stop client on intf %s", info->intf_path, info->intf_name);
        amxp_timer_stop(info->timer_physdown);

        dm_dhcpv4client_start_stop_client(info, false);
        free(info->intf_name);
        info->intf_name = NULL;
    }
    // else interface have changed, will be handle by the isup query

exit:
    return;
}

void client_info_clear(client_info_t* info) {
    when_null(info, exit);

    dhcpv4c_delay_clean(&(info->update_timer));
    amxp_timer_delete(&(info->timer_physdown));
    if(info->q_name != NULL) {
        dm_dhcpv4client_start_stop_client(info, false);
        netmodel_closeQuery(info->q_name);
        info->q_name = NULL;
    }
    if(info->q_isup != NULL) {
        netmodel_closeQuery(info->q_isup);
        info->q_isup = NULL;
    }
    if(info->obj != NULL) {
        info->obj->priv = NULL;
    }
    free(info->intf_name);
    info->intf_name = NULL;
    free(info->intf_path);
    info->intf_path = NULL;
    free(info);
exit:
    return;
}

void client_info_update(amxd_object_t* obj, bool enabled) {
    client_info_t* info = NULL;
    when_null(obj, exit);
    when_null(obj->priv, exit);
    info = (client_info_t*) obj->priv;
    if(enabled == true) {
        if(info->q_name != NULL) {
            netmodel_closeQuery(info->q_name);
        }
        info->q_name = netmodel_openQuery_getFirstParameter((const char*) info->intf_path,
                                                            "tr181-dhcpv4client", "NetDevName",
                                                            "ipv4",
                                                            netmodel_traverse_down,
                                                            client_intf_netmodel_cb, (void*) info);
        if(info->q_name != NULL) {
            SAH_TRACEZ_INFO(ME, "Query to get NetDevName for intf %s ok",
                            info->intf_path);
        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to add a query to netmodel to get NetDevName");
        }

        if(info->q_isup != NULL) {
            netmodel_closeQuery(info->q_isup);
        }
        info->q_isup = netmodel_openQuery_getFirstParameter((const char*) info->intf_path,
                                                            "tr181-dhcpv4client", "NetDevName",
                                                            "netdev-up && ipv4",
                                                            netmodel_traverse_down,
                                                            client_isup_netmodel_cb, (void*) info);
        if(info->q_isup != NULL) {
            SAH_TRACEZ_INFO(ME, "Query to get NetDevNamewhen interface up for intf %s ok",
                            info->intf_path);
        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to add a query to netmodel to get NetDevName when interface up");
        }
    } else {
        amxp_timer_stop(info->timer_physdown);

        dm_dhcpv4client_start_stop_client(info, false);
        if(info->q_name != NULL) {
            netmodel_closeQuery(info->q_name);
            info->q_name = NULL;
        }
        if(info->q_isup != NULL) {
            netmodel_closeQuery(info->q_isup);
            info->q_isup = NULL;
        }
        SAH_TRACEZ_INFO(ME, "free ifname %s,intf %s", info->intf_name,
                        info->intf_path);
        free(info->intf_name);
        info->intf_name = NULL;
    }
exit:
    return;
}

void client_info_create(amxd_object_t* obj, bool update) {
    client_info_t* info = NULL;
    char* intf_path = NULL;
    when_false_trace(obj->priv == NULL, exit, ERROR, "Failed to subscribe on netmodel," \
                     " priv was not null. For object: %s", obj->name);
    intf_path = amxd_object_get_value(cstring_t, obj, "Interface", NULL);
    if((intf_path == NULL) || (*intf_path == '\0')) {
        SAH_TRACEZ_INFO(ME, "Interface path is empty for DHCPv4Client.Client.%d", obj->index);
        free(intf_path);
        goto exit;
    }
    info = (client_info_t*) calloc(1, sizeof(client_info_t));
    obj->priv = info;
    info->obj = obj;
    info->intf_path = intf_path;
    dhcpv4c_delay_init(&(info->update_timer), info->intf_path);
    amxp_timer_new(&(info->timer_physdown), dhcpv4c_delay_stop_phys_down, info);

    // check if configuration has to be saved
    info->update = update;
    if(amxd_object_get_value(bool, obj, "Enable", NULL) == true) {
        client_info_update(obj, true);
    }
exit:
    return;
}
