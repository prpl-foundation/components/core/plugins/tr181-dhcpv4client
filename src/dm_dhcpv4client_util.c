/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "dm_dhcpv4client.h"
#include "dhcpv4c_config.h"
#include "dhcpv4c_delay_update.h"
#include "firewall.h"

const char* str_status[] = {
    "Disabled",
    "Enabled",
    "Error_Misconfigured"
};

const char* str_dhcp_status[] = {
    "Init",
    "Selecting",
    "Requesting",
    "Rebinding",
    "Bound",
    "Renewing"
};

static amxc_var_t dm_storage;

bool dm_dhcpv4client_is_status_supported(const char* status) {
    bool ret = false;
    unsigned int size = sizeof(str_dhcp_status) / sizeof(str_dhcp_status[0]);
    unsigned int i = 0;

    when_str_empty(status, exit);

    for(i = 0; i < size; i++) {
        when_true_status(strcmp(status, str_dhcp_status[i]) == 0, exit, ret = true);
    }

exit:
    return ret;
}

void dm_dhcpv4client_client_status(amxd_trans_t* trans,
                                   int status,
                                   int dhcpstatus) {
    if((status >= 0) && (status < dhcpv4c_status_last)) {
        amxd_trans_set_value(cstring_t, trans,
                             "Status", str_status[status]);
    }
    if((dhcpstatus >= 0) && (dhcpstatus < dhcpv4c_dhcp_status_last)) {
        amxd_trans_set_value(cstring_t, trans,
                             "DHCPStatus", str_dhcp_status[dhcpstatus]);
    }
}

void dm_dhcpv4client_init_client(amxd_object_t* object,
                                 int status,
                                 int dhcp_status) {
    amxd_object_t* reqopts = amxd_object_findf(object, ".ReqOption.");
    amxd_trans_t* trans = NULL;

    amxd_trans_new(&trans);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(trans, object);

    dm_dhcpv4client_client_status(trans, status, dhcp_status);

    amxd_trans_set_value(cstring_t, trans, "IPAddress", "");
    amxd_trans_set_value(cstring_t, trans, "DNSServers", "");
    amxd_trans_set_value(cstring_t, trans, "DHCPServer", "");
    amxd_trans_set_value(cstring_t, trans, "PassthroughDHCPPool", "");
    amxd_trans_set_value(int32_t, trans, "LeaseTimeRemaining", 0);
    amxd_trans_set_value(cstring_t, trans, "IPRouters", "");
    amxd_trans_set_value(cstring_t, trans, "SubnetMask", "");
    amxd_object_for_each(instance, itl, reqopts) {
        amxd_object_t* req_opts = amxc_container_of(itl, amxd_object_t, it);
        amxd_trans_select_object(trans, req_opts);
        amxd_trans_set_value(cstring_t, trans, "Value", "");
    }
    if(amxd_trans_apply(trans, dhcpv4client_get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_WARNING(ME, "Failed to apply transaction");
    }
    amxd_trans_delete(&trans);
}

void dm_dhcpv4client_start_stop_client(client_info_t* info,
                                       bool start) {
    const char* func = start ? "dhcpv4c-start" : "dhcpv4c-stop";
    const char* module = NULL;
    int retval = -1;
    amxc_var_t result;
    amxc_var_t data;
    amxc_var_t params;

    amxc_var_init(&result);
    amxc_var_init(&data);
    amxc_var_init(&params);

    when_null(info, exit);
    when_null(info->obj, exit);

    amxd_object_get_params(info->obj, &params, amxd_dm_access_private);

    module = GET_CHAR(&params, "Controller");
    when_str_empty_trace(module, exit, INFO, "no Controller specified");
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", info->intf_path);
    amxc_var_add_key(cstring_t, &data, "IfName", info->intf_name);
    dhcpv4c_config_add_options(info->obj, &data, true);
    dhcpv4c_config_add_options(info->obj, &data, false);
    amxc_var_set_key(&data, "RetransmissionStrategy",
                     GET_ARG(&params, "RetransmissionStrategy"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&data, "RetransmissionRandomize",
                     GET_ARG(&params, "RetransmissionRandomize"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&data, "RetransmissionRenewTimeout",
                     GET_ARG(&params, "RetransmissionRenewTimeout"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&data, "CheckAuthentication",
                     GET_ARG(&params, "CheckAuthentication"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&data, "AuthenticationInformation",
                     GET_ARG(&params, "AuthenticationInformation"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&data, "ReleaseOnReboot",
                     GET_ARG(&params, "ReleaseOnReboot"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&data, "storage-path",
                     GET_ARG(dhcpv4client_get_config(), "storage-path"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(&data, "DSCPMark",
                     GET_ARG(&params, "DSCPMark"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&data, "PriorityMark",
                     GET_ARG(&params, "PriorityMark"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&data, "BroadcastFlag",
                     GET_ARG(&params, "BroadcastFlag"), AMXC_VAR_FLAG_DEFAULT);

    retval = amxm_execute_function(module, module, func, &data, &result);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "%s,  execute function '%s' returned %d",
                         module, func, retval);
        goto exit;
    }
    retval = GET_INT32(&result, NULL);
    SAH_TRACEZ_INFO(ME, "%s func '%s' returned %d",
                    module, func, retval);
exit:
    amxc_var_clean(&result);
    amxc_var_clean(&data);
    amxc_var_clean(&params);
}

void dm_dhcpv4client_start(client_info_t* info) {
    char* alias = NULL;

    when_null(info, exit);
    alias = amxd_object_get_value(cstring_t, info->obj, "Alias", NULL);
    firewall_open_dhcp_port(info->intf_path, alias);

    dm_dhcpv4client_start_stop_client(info, true);
exit:
    free(alias);
    return;
}

void dm_update_client_deconfig(amxd_object_t* dm_client) {

    dm_dhcpv4client_init_client(dm_client, dhcpv4c_status_disabled,
                                dhcpv4c_dhcp_status_init);
    if(amxd_object_get_value(bool, dm_client, "Enable", NULL) == false) {
        char* interface = amxd_object_get_value(cstring_t, dm_client,
                                                "Interface", NULL);
        char* alias = amxd_object_get_value(cstring_t, dm_client,
                                            "Alias", NULL);
        firewall_close_dhcp_port(interface, alias);
        free(interface);
        free(alias);
    }
}

void dm_update_client_bound(amxd_object_t* dm_client,
                            amxc_var_t* params) {
    amxd_trans_t* trans = NULL;
    client_info_t* info = NULL;

    amxd_trans_new(&trans);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(trans, dm_client);

    when_null_trace(dm_client, exit, ERROR, "dm_client is NULL");

    info = (client_info_t*) dm_client->priv;
    when_null_trace(info, exit, ERROR, "NULL ptr for info_client, alias <%s>",
                    amxd_object_get_name(dm_client, AMXD_OBJECT_NAMED));

    amxc_var_for_each(var, params) {
        const char* key = amxc_var_key(var);
        if((strcmp(key, "bound") == 0) || (strcmp(key, "ifname") == 0) ||
           (strcmp(key, "status") == 0) || (strcmp(key, "error") == 0)) {
            continue;
        }
        if(strcmp(key, "ReqOption") == 0) {
            amxc_var_t* opt = GET_ARG(params, "ReqOption");
            dhcpv4c_config_options(trans, opt, dm_client, true, true);
            continue;
        }
        if(strcmp(key, "LeaseTimeRemaining") == 0) {
            amxc_ts_now(&info->lease_timer);
        }
        SAH_TRACEZ_INFO(ME, "parameter '%s', value '%s'",
                        key, GET_CHAR(params, key));
        amxd_trans_set_value(cstring_t, trans, key, GET_CHAR(params, key));
    }

    if(amxd_trans_apply(trans, dhcpv4client_get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_WARNING(ME, "Failed to apply transaction");
    }
    amxd_trans_clean(trans);
    amxd_trans_select_object(trans, dm_client);
    dm_dhcpv4client_client_status(trans, dhcpv4c_status_enabled,
                                  dhcpv4c_dhcp_status_bound);
    if(amxd_trans_apply(trans, dhcpv4client_get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_WARNING(ME, "Failed to apply transaction");
    }

exit:
    amxd_trans_delete(&trans);
    return;
}

void dm_update_client_config(const char* path) {
    client_info_t* info = NULL;
    char* client_path = strdup(path);
    char* pos = NULL;
    amxd_object_t* dm_client = NULL;
    // Begin searching from position 20 (x character, "DHCPv4Client.Client.x")
    // for next occurence of decimal point
    pos = strchr(&client_path[20], (int) '.');
    when_null(pos, exit);
    *pos = '\0';
    dm_client = amxd_dm_findf(dhcpv4client_get_dm(), "%s.", client_path);
    when_null_trace(dm_client, exit, WARNING,
                    "got null ptr for client, path <%s>", path);
    info = (client_info_t*) dm_client->priv;
    when_null_trace(info, exit, WARNING,
                    "null ptr for info_client, path <%s>", path);
    dhcpv4c_delay_update(info->update_timer);
exit:
    free(client_path);
}

void dm_storage_store_data(const char* key, amxc_var_t* data) {
    amxc_var_set_key(&dm_storage, key, data, AMXC_VAR_FLAG_COPY | AMXC_VAR_FLAG_UPDATE);
}

amxc_var_t* dm_storage_get_data(const char* key) {
    return amxc_var_take_key(&dm_storage, key);
}

void dm_storage_init(void) {
    amxc_var_init(&dm_storage);
    amxc_var_set_type(&dm_storage, AMXC_VAR_ID_HTABLE);
}

void dm_storage_clean(void) {
    amxc_var_clean(&dm_storage);
}
