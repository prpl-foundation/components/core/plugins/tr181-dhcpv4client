#!/bin/sh
[ -z "$1" ] && echo "Error: should be run by udhcpc" && exit 1

json=''
IFS=$'\n'
for s in `env`; do
  # get key name
  key=${s%%=*}
  # get rest of line, remove first character ('=') and " \ characters
  value=${s#$key}
  value=${value:1}
  value=${value//\"/}
  value=${value//'\'/}
  json=$json', "'$key'": "'$value'"'
done

data='"action": "'$1'"'$json
fncdata='{ "mod_name": "mod-udhcpc-access", "fnc": "user-script", "data": { '$data' } }'

case "$1" in
        renew|bound|deconfig)
                ubus wait_for DHCPv4Client
                ubus call DHCPv4Client update "$fncdata"
        ;;
esac

exit 0
