#!/bin/sh
string_to_hex() {
  printf '%s' "$1" | hexdump -ve '/1 "%02X"'
}

if [ -f "/var/etc/environment" ]; then
    source /var/etc/environment
fi

#if enterprise number is empty default to 0000
ENTERPRISE_NUMBER="$PEN"
if [ -z $ENTERPRISE_NUMBER ]; then
    ENTERPRISE_NUMBER=0000
fi

# Convert sub-option values to hexadecimal 4 bytes encoded
ENTERPRISE_NUMBER_HEX=$(echo -n "$ENTERPRISE_NUMBER" | awk '{ printf("%08x\n", $1) }')

# Convert sub-option values to hexadecimal
MANUFACTURER_OUI_HEX=$(string_to_hex "$MANUFACTUREROUI")
SERIAL_NUMBER_HEX=$(string_to_hex "$SERIALNUMBER")
PRODUCT_CLASS_HEX=$(string_to_hex "$PRODUCTCLASS")

MANUFACTURER_OUI_LEN=$(echo -n "$MANUFACTUREROUI" | wc -c | awk '{ printf("%02x\n", $1) }')
SERIAL_NUMBER_LEN=$(echo -n "$SERIALNUMBER" | wc -c | awk '{ printf("%02x\n", $1) }')
PRODUCT_CLASS_LEN=$(echo -n "$PRODUCTCLASS" | wc -c | awk '{ printf("%02x\n", $1) }')

#For Repeater change 04 -> 01 , 05 -> 02 , 06 -> 03 (it should be included in the dhcpv4-client in that case)
GW_DHCPV4_OPTION_125_VALUE_DATA="04${MANUFACTURER_OUI_LEN}${MANUFACTURER_OUI_HEX}05${SERIAL_NUMBER_LEN}${SERIAL_NUMBER_HEX}06${PRODUCT_CLASS_LEN}${PRODUCT_CLASS_HEX}"
GW_DHCPV4_OPTION_125_VALUE_DATA_LEN=$(echo -n "$GW_DHCPV4_OPTION_125_VALUE_DATA" | wc -c | awk '{ printf("%02x\n", $1/2) }')
GW_DHCPV4_OPTION_125_VALUE="${ENTERPRISE_NUMBER_HEX}${GW_DHCPV4_OPTION_125_VALUE_DATA_LEN}${GW_DHCPV4_OPTION_125_VALUE_DATA}"

LAN_DHCPV4_OPTION_125_VALUE_DATA="01${MANUFACTURER_OUI_LEN}${MANUFACTURER_OUI_HEX}02${SERIAL_NUMBER_LEN}${SERIAL_NUMBER_HEX}03${PRODUCT_CLASS_LEN}${PRODUCT_CLASS_HEX}"
LAN_DHCPV4_OPTION_125_VALUE_DATA_LEN=$(echo -n "$LAN_DHCPV4_OPTION_125_VALUE_DATA" | wc -c | awk '{ printf("%02x\n", $1/2) }')
LAN_DHCPV4_OPTION_125_VALUE="${ENTERPRISE_NUMBER_HEX}${LAN_DHCPV4_OPTION_125_VALUE_DATA_LEN}${LAN_DHCPV4_OPTION_125_VALUE_DATA}"

echo "export GW_DHCPV4_OPTION_125_VALUE=\"$GW_DHCPV4_OPTION_125_VALUE\"" >> /var/etc/environment
echo "export LAN_DHCPV4_OPTION_125_VALUE=\"$LAN_DHCPV4_OPTION_125_VALUE\"" >> /var/etc/environment