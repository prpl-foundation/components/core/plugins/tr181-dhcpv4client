/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>

#include "dhcpv4client.h"
#include "dm_dhcpv4client.h"
#include "../common/common_functions.h"

#define UNUSED __attribute__((unused))

void resolver_add_all_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv4client_instance_added",
                                            AMXO_FUNC(_dhcpv4client_instance_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv4client_enable_changed",
                                            AMXO_FUNC(_dhcpv4client_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "update",
                                            AMXO_FUNC(_update)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "renew_lease",
                                            AMXO_FUNC(_renew_lease)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "return_false",
                                            AMXO_FUNC(_return_false)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv4c_option_changed",
                                            AMXO_FUNC(_dhcpv4c_option_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv4c_object_changed",
                                            AMXO_FUNC(_dhcpv4c_object_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "object_delete",
                                            AMXO_FUNC(_object_delete)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "client_object_delete",
                                            AMXO_FUNC(_client_object_delete)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "read_lease_time_remaining",
                                            AMXO_FUNC(_read_lease_time_remaining)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv4c_timer_changed",
                                            AMXO_FUNC(_dhcpv4c_timer_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv4c_config_changed",
                                            AMXO_FUNC(_dhcpv4c_config_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_read",
                                            AMXO_FUNC(_stats_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_list",
                                            AMXO_FUNC(_stats_list)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_describe",
                                            AMXO_FUNC(_stats_describe)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ClearStatistics",
                                            AMXO_FUNC(_ClearStatistics)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_tag_not_enabled",
                                            AMXO_FUNC(_check_tag_not_enabled)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_tag_already_enabled",
                                            AMXO_FUNC(_check_tag_already_enabled)), 0);

    // RPCs
    assert_int_equal(amxo_resolver_ftab_add(parser, "Renew",
                                            AMXO_FUNC(_Renew)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Release",
                                            AMXO_FUNC(_Release)), 0);
}

/**
   @brief
   Search for a string in a linked list.

   @param[in] string String to be searched.
   @param[in] llist Linked list.

   @return
   false if the list doesn't contain the string.
 */
UNUSED bool dhcpv4c_search_llist(cstring_t string, amxc_llist_t* llist) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* list_string = NULL;
    bool ret = false;

    when_null_trace(string, exit, ERROR, "String is NULL.");

    amxc_llist_for_each(it, llist) {
        list_string = amxc_container_of(it, amxc_string_t, it);
        if(strcmp(string, amxc_string_get(list_string, 0)) == 0) {
            ret = true;
            break;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return ret;
}

void obj_data_to_var(amxd_object_t* object, amxc_llist_t* filter_list, amxc_var_t* var) {
    SAH_TRACEZ_IN(ME);
    amxd_param_t* param = NULL;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* tmp_var = NULL;
    cstring_t obj_name = NULL;
    cstring_t param_name = NULL;
    uint32_t index = 1;
    uint32_t inst_count = 0;
    amxc_string_t* str_index;
    amxd_object_type_t obj_type;

    amxc_string_new(&str_index, 0);

    when_null_trace(object, exit, ERROR, "The object is NULL.");
    when_null_trace(var, exit, ERROR, "Variant is not initialised.");

    obj_type = amxd_object_get_type(object);
    if(obj_type == amxd_object_template) {
        inst_count = amxd_object_get_instance_count(object);
        if(inst_count > 0) {
            amxc_llist_for_each(it, (&object->instances)) {
                tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
                index = amxd_object_get_index(tmp_obj);
                amxc_string_setf(str_index, "%u", index);
                tmp_var = amxc_var_add_key(amxc_htable_t, var, amxc_string_get(str_index, 0), NULL);
                obj_data_to_var(tmp_obj, filter_list, tmp_var);
                tmp_obj = NULL;
            }
        }
    } else {
        amxc_llist_for_each(it, (&object->parameters)) {
            param = amxc_llist_it_get_data(it, amxd_param_t, it);
            param_name = (cstring_t) amxd_param_get_name(param);
            if(dhcpv4c_search_llist(param_name, filter_list) == false) {
                tmp_var = (amxc_var_t*) amxd_object_get_param_value(object, param_name);
                if((amxc_var_type_of(tmp_var) != AMXC_VAR_ID_HTABLE) && \
                   (amxc_var_type_of(tmp_var) != AMXC_VAR_ID_LIST)) {
                    amxc_var_set_pathf(var, tmp_var, DEFAULT_SET_PATH_FLAGS, "%s", param_name);
                }
            }
            param = NULL;
        }
        amxc_llist_for_each(it, (&object->objects)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            if(dhcpv4c_search_llist(obj_name, filter_list) == false) {
                tmp_var = amxc_var_add_key(amxc_htable_t, var, obj_name, NULL);
                obj_data_to_var(tmp_obj, filter_list, tmp_var);
            }
            tmp_obj = NULL;
        }
    }
exit:
    amxc_string_delete(&str_index);
    SAH_TRACEZ_OUT(ME);
}