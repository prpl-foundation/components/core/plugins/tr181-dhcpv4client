/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include <amxo/amxo.h>

#include "dhcpv4client.h"

#include "../../include_priv/dhcpv4client.h"
#include "../../include_priv/dm_dhcpv4client.h"
#include "test_dhcpv4client_events.h"

#include "../common/mock.h"
#include "../common/common_functions.h"

#define DHCPV4_TEST "../common/dhcpv4client_test.odl"
#define DHCPV4_DEFAULT "../common/dhcpv4client_default.odl"
#define DUMMY_TEST_ODL "../common/dummy.odl"

int test_setup(void** state) {
    amxo_parser_t* parser = NULL;

    amxut_bus_setup(state);

    parser = amxut_bus_parser();

    amxo_resolver_ftab_add(parser, "leases", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(parser, "get", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(parser, "commit", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(parser, "set", AMXO_FUNC(dummy_function_set));

    resolver_add_all_functions(parser);

    amxut_dm_load_odl(DHCPV4_TEST);
    amxut_dm_load_odl(DUMMY_TEST_ODL);

    _dummy_main(0, amxut_bus_dm(), parser);
    dummy_set_file(UCI_GET, "../common/data/uci_get_network_interface.json", "network_interface");

    amxut_bus_handle_events();

    assert_int_equal(_dhcpv4client_main(0, amxut_bus_dm(), parser), 0);

    amxut_dm_load_odl(DHCPV4_DEFAULT);

    amxut_bus_handle_events();
    return 0;
}

void test_renew_lease(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.1.");
    amxd_trans_t transaction;

    assert_non_null(dhcpv4client);

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_set_value(bool, &transaction, "Renew", true);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    assert_false(amxd_object_get_bool(dhcpv4client, "Renew", &ret_status));
}

void test_release_lease(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.1.");
    amxd_trans_t transaction;

    assert_non_null(dhcpv4client);

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_set_value(bool, &transaction, "Release", true);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    assert_false(amxd_object_get_bool(dhcpv4client, "Release", &ret_status));
}

void test_dhcpv4client_enable_changed(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.1.");
    amxd_trans_t transaction;

    assert_non_null(dhcpv4client);

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_set_value(bool, &transaction, "Enable", false);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);

    assert_false(amxd_object_get_bool(dhcpv4client, "Enable", &ret_status));

    ret_status = amxd_trans_set_value(bool, &transaction, "Enable", true);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    assert_true(amxd_object_get_bool(dhcpv4client, "Enable", &ret_status));

    amxd_trans_clean(&transaction);
}

void test_dhcpv4c_option_changed(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* reqoption = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.1.ReqOption.1.");
    amxd_trans_t transaction;

    assert_non_null(reqoption);

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_select_object(&transaction, reqoption);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_set_value(bool, &transaction, "Enable", false);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_select_object(&transaction, reqoption);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_set_value(bool, &transaction, "Enable", true);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    assert_true(amxd_object_get_bool(reqoption, "Enable", &ret_status));
}

void test_dhcpv4c_object_changed(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.1.");
    amxd_trans_t transaction;
    char* interface = NULL;

    assert_non_null(dhcpv4client);

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_set_value(cstring_t, &transaction, "Interface", "Device.IP.Interface.3.");
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_set_value(cstring_t, &transaction, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    interface = amxd_object_get_cstring_t(dhcpv4client, "Interface", &ret_status);

    assert_string_equal(interface, "Device.IP.Interface.2.");
    free(interface);
}

void test_dhcpv4c_timer_changed(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.1.");
    amxd_trans_t transaction;
    int timeout = 0;

    assert_non_null(dhcpv4client);

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_set_value(int32_t, &transaction, "ResetOnPhysDownTimeout", 1500);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_set_value(int32_t, &transaction, "ResetOnPhysDownTimeout", 1000);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    timeout = amxd_object_get_int32_t(dhcpv4client, "ResetOnPhysDownTimeout", &ret_status);

    assert_int_equal(timeout, 1000);
}

void test_dhcpv4c_config_changed(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.1.");
    amxd_trans_t transaction;

    assert_non_null(dhcpv4client);

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_set_value(bool, &transaction, "ReleaseOnReboot", true);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxd_trans_clean(&transaction);

    amxut_bus_handle_events();

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);

    assert_true(amxd_object_get_bool(dhcpv4client, "ReleaseOnReboot", &ret_status));

    ret_status = amxd_trans_set_value(bool, &transaction, "ReleaseOnReboot", false);
    assert_int_equal(ret_status, 0);

    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    assert_false(amxd_object_get_bool(dhcpv4client, "ReleaseOnReboot", &ret_status));

    amxd_trans_clean(&transaction);
}

int test_teardown(void** state) {
    assert_int_equal(_dhcpv4client_main(1, amxut_bus_dm(), amxut_bus_parser()), 0);
    _dummy_main(0, amxut_bus_dm(), amxut_bus_parser());
    amxut_bus_handle_events();
    amxut_bus_teardown(state);
    return 0;
}