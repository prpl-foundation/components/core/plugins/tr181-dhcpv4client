/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include <amxo/amxo.h>

#include "dhcpv4client.h"

#include "../../include_priv/dhcpv4client.h"
#include "../../include_priv/dm_dhcpv4client.h"
#include "test_dhcpv4client_method.h"

#include "../common/mock.h"
#include "../common/common_functions.h"

#define DHCPV4_TEST "../common/dhcpv4client_test.odl"
#define DHCPV4_DEFAULT "../common/dhcpv4client_default.odl"
#define DUMMY_TEST_ODL "../common/dummy.odl"

int test_setup(void** state) {
    amxo_parser_t* parser = NULL;

    amxut_bus_setup(state);

    parser = amxut_bus_parser();

    amxo_resolver_ftab_add(parser, "leases", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(parser, "get", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(parser, "commit", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(parser, "set", AMXO_FUNC(dummy_function_set));

    resolver_add_all_functions(parser);

    amxut_dm_load_odl(DHCPV4_TEST);
    amxut_dm_load_odl(DUMMY_TEST_ODL);

    _dummy_main(0, amxut_bus_dm(), parser);
    dummy_set_file(UCI_GET, "../common/data/uci_get_network_interface.json", "network_interface");

    amxut_bus_handle_events();

    assert_int_equal(_dhcpv4client_main(0, amxut_bus_dm(), parser), 0);

    amxut_dm_load_odl(DHCPV4_DEFAULT);

    amxut_bus_handle_events();
    return 0;
}


void test_update_method(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    cstring_t mod_name = "mod-uci-access";
    cstring_t fnc = "dhcpv4c-config-update";
    amxc_var_t args;
    amxc_var_t ret;
    amxc_htable_t data;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.");

    assert_non_null(dhcpv4client);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_htable_init(&data, 0);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "mod_name", mod_name);
    amxc_var_add_key(cstring_t, &args, "fnc", fnc);
    amxc_var_add_key(amxc_htable_t, &args, "data", &data);

    ret_status = amxd_object_invoke_function(dhcpv4client, "update", &args, &ret);
    assert_int_equal(amxd_status_ok, ret_status);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_htable_clean(&data, NULL);
}

void test_renew_method(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.wan.");

    assert_non_null(dhcpv4client);

    ret_status = _Renew(dhcpv4client, NULL, NULL, NULL);
    assert_int_equal(amxd_status_ok, ret_status);

    ret_status = _Renew(NULL, NULL, NULL, NULL);
    assert_int_equal(amxd_status_unknown_error, ret_status);
}

void test_release_method(UNUSED void** state) {
    amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.wan.");

    assert_non_null(dhcpv4client);

    ret_status = _Release(dhcpv4client, NULL, NULL, NULL);
    assert_int_equal(amxd_status_ok, ret_status);

    ret_status = _Release(NULL, NULL, NULL, NULL);
    assert_int_equal(amxd_status_unknown_error, ret_status);
}

// This test will trigger the function "test_dm_update_client" manually. See if any
// memory leaks remain or if the basic beahvior of the function is right...
void test_dm_update_client(UNUSED void** state) {
    UNUSED amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.1.");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* data = NULL;
    amxc_var_t* var_status = NULL;
    amxc_var_t* var_bound = NULL;
    amxc_var_t* var_error = NULL;
    amxc_var_t* var_reqoption = NULL;
    amxc_var_t* var_theopt = NULL;
    amxd_trans_t transaction;
    char* IfName = NULL;
    char* str_status = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    // Setting the status to Bound
    data = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, data, "ifname", "eth0");
    var_status = amxc_var_add_key(cstring_t, data, "status", "Selecting");
    var_error = amxc_var_add_key(cstring_t, data, "error", "None");
    var_bound = amxc_var_add_key(bool, data, "bound", true);
    var_reqoption = amxc_var_add_key(amxc_htable_t, data, "ReqOption", NULL);
    var_theopt = amxc_var_add_key(amxc_htable_t, var_reqoption, "1", NULL);
    amxc_var_add_key(bool, var_theopt, "Enable", false);
    amxc_var_add_key(uint32_t, var_theopt, "Tag", 58);
    var_theopt = amxc_var_add_key(amxc_htable_t, var_reqoption, "3", NULL);
    amxc_var_add_key(bool, var_theopt, "Enable", true);
    amxc_var_add_key(uint32_t, var_theopt, "Tag", 16);

    assert_int_equal(dm_update_client(__func__, &args, &ret), -1);

    ret_status = amxd_trans_init(&transaction);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_select_object(&transaction, dhcpv4client);
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_set_value(cstring_t, &transaction, "IfName", "eth0");
    assert_int_equal(ret_status, 0);
    ret_status = amxd_trans_apply(&transaction, amxut_bus_dm());
    assert_int_equal(ret_status, 0);

    amxut_bus_handle_events();

    // Setting the IfName parameter and testing the status
    IfName = amxd_object_get_value(cstring_t, dhcpv4client, "IfName", &ret_status);
    assert_string_equal(IfName, "eth0");
    assert_int_equal(dm_update_client(__func__, &args, &ret), 0);
    str_status = amxd_object_get_value(cstring_t, dhcpv4client, "Status", &ret_status);
    assert_string_equal(str_status, "Enabled");

    // Setting the status to "Init"
    amxc_var_set(cstring_t, var_status, "Init");
    assert_int_equal(dm_update_client(__func__, &args, &ret), 0);

    // Setting the status to "Idle"
    amxc_var_set(cstring_t, var_status, "Idle");
    assert_int_equal(dm_update_client(__func__, &args, &ret), 0);

    // Setting the status to "Selecting" as an edge case
    amxc_var_set(cstring_t, var_status, "Selecting");
    assert_int_equal(dm_update_client(__func__, &args, &ret), 0);

    // Setting the status to NULL
    amxc_var_clean(var_status);
    assert_int_equal(dm_update_client(__func__, &args, &ret), 0);
    // setting the bound to false
    amxc_var_set(bool, var_bound, false);
    assert_int_equal(dm_update_client(__func__, &args, &ret), 0);

    // Setting the error to "Authentication Failure"
    free(str_status);
    amxc_var_set(cstring_t, var_status, "Selecting");
    amxc_var_set(cstring_t, var_error, "Authentication Failure");
    assert_int_equal(dm_update_client(__func__, &args, &ret), 0);
    str_status = amxd_object_get_value(cstring_t, dhcpv4client, "Status", &ret_status);
    assert_string_equal(str_status, "Error_Misconfigured");

    // Setting the error to "LocalRelease"
    free(str_status);
    amxc_var_set(cstring_t, var_error, "LocalRelease");
    assert_int_equal(dm_update_client(__func__, &args, &ret), 0);
    str_status = amxd_object_get_value(cstring_t, dhcpv4client, "Status", &ret_status);
    assert_string_equal(str_status, "Disabled");

    amxd_trans_clean(&transaction);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    free(IfName);
    free(str_status);
}

// This test will trigger the function "dm_client_misconfigured" manually. See if any
// memory leaks remain or if the basic behavior of the function is right...
void test_dm_client_misconfigured(UNUSED void** state) {
    UNUSED amxd_status_t ret_status = amxd_status_ok;
    amxd_object_t* dhcpv4client = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Client.Client.1.");
    amxc_var_t args;
    amxc_var_t ret;
    char* status_char = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    // Setting the interface of the client to set to missconfigured
    amxc_var_add_key(cstring_t, &args, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(dm_client_misconfigured(__func__, &args, &ret), 0);
    status_char = amxd_object_get_value(cstring_t, dhcpv4client, "Status", &ret_status);
    assert_string_equal(status_char, "Error_Misconfigured");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(status_char);
}

int test_teardown(void** state) {
    assert_int_equal(_dhcpv4client_main(1, amxut_bus_dm(), amxut_bus_parser()), 0);
    _dummy_main(0, amxut_bus_dm(), amxut_bus_parser());
    amxut_bus_handle_events();
    amxut_bus_teardown(state);
    return 0;
}