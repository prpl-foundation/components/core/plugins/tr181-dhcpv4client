/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DM_DHCPV4CLIENT_H__)
#define __DM_DHCPV4CLIENT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "dhcpv4client.h"
#include "client_info.h"

typedef enum _dhcpv4c_status {
    dhcpv4c_status_disabled,
    dhcpv4c_status_enabled,
    dhcpv4c_status_error_misconfigure,
    dhcpv4c_status_last
} dhcpv4c_status_t;

typedef enum _dhcpv4c_dhcp_status {
    dhcpv4c_dhcp_status_init,
    dhcpv4c_dhcp_status_selecting,
    dhcpv4c_dhcp_status_requesting,
    dhcpv4c_dhcp_status_rebinding,
    dhcpv4c_dhcp_status_bound,
    dhcpv4c_dhcp_status_renewing,
    dhcpv4c_dhcp_status_last
} dhcpv4c_dhcp_status_t;

// data model event handlers
void _print_event(const char* const event_name,
                  const amxc_var_t* const event_data,
                  void* const priv);
void _dhcpv4client_instance_added(const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  void* const priv);

void _dhcpv4client_instance_updated(const char* const event_name,
                                    const amxc_var_t* const event_data,
                                    void* const priv);

void _dhcpv4client_instance_removed(const char* const event_name,
                                    const amxc_var_t* const event_data,
                                    void* const priv);

void _dhcpv4client_enable_changed(const char* const event_name,
                                  const amxc_var_t* const event_data,
                                  void* const priv);
void _dhcpv4c_option_changed(const char* const event_name,
                             const amxc_var_t* const event_data,
                             void* const priv);
void _dhcpv4c_object_changed(const char* const event_name,
                             const amxc_var_t* const event_data,
                             void* const priv);
void _dhcpv4c_timer_changed(const char* const event_name,
                            const amxc_var_t* const event_data,
                            void* const priv);
void _dhcpv4c_config_changed(const char* const event_name,
                             const amxc_var_t* const event_data,
                             void* const priv);
// object actions
amxd_status_t _object_delete(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);
amxd_status_t _client_object_delete(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv);
amxd_status_t _stats_read(amxd_object_t* object,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv);
amxd_status_t _stats_list(amxd_object_t* object,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv);
amxd_status_t _stats_describe(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv);
amxd_status_t _ClearStatistics(amxd_object_t* object,
                               amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret);
amxd_status_t _return_false(amxd_object_t* object,
                            amxd_param_t* param,
                            amxd_action_t reason,
                            const amxc_var_t* const args,
                            amxc_var_t* const retval,
                            void* priv);

amxd_status_t _renew_lease(amxd_object_t* object,
                           amxd_param_t* param,
                           amxd_action_t reason,
                           const amxc_var_t* const args,
                           amxc_var_t* const retval,
                           void* priv);

amxd_status_t _release_lease(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);

amxd_status_t _read_lease_time_remaining(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv);

amxd_status_t _check_tag_not_enabled(amxd_object_t* object,
                                     amxd_param_t* param,
                                     amxd_action_t reason,
                                     const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     void* priv);

amxd_status_t _check_tag_already_enabled(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv);
// data model methods
amxd_status_t _update(amxd_object_t* object,
                      amxd_function_t* func,
                      amxc_var_t* args,
                      amxc_var_t* ret);
amxd_status_t _Renew(amxd_object_t* object,
                     amxd_function_t* func,
                     amxc_var_t* args,
                     amxc_var_t* ret);
amxd_status_t _Release(amxd_object_t* object,
                       amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret);

// data model utility functions
bool dm_dhcpv4client_is_status_supported(const char* status);
void dm_dhcpv4client_client_status(amxd_trans_t* transaction,
                                   int status,
                                   int dhcpstatus);
void dm_dhcpv4client_init_client(amxd_object_t* object,
                                 int status,
                                 int dhcp_status);
void dm_dhcpv4client_start_stop_client(client_info_t* info,
                                       bool start);
void dm_dhcpv4client_start(client_info_t* info);
void dm_update_client_deconfig(amxd_object_t* dm_client);
void dm_update_client_bound(amxd_object_t* dm_client,
                            amxc_var_t* params);
void dm_update_client_config(const char* path);
int dhcpv4c_mod_renew_release(const char* mod_name,
                              const char* intf_name,
                              bool renew);

void dm_storage_store_data(const char* key, amxc_var_t* data);
amxc_var_t* dm_storage_get_data(const char* key);
void dm_storage_init(void);
void dm_storage_clean(void);

// registered functions
int dm_update_client(const char* function_name,
                     amxc_var_t* args,
                     amxc_var_t* ret);
int dm_client_misconfigured(const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret);

#ifdef __cplusplus
}
#endif

#endif // __DM_DHCPV4CLIENT_H__
