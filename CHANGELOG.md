# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.26.9 - 2024-09-24(13:52:37 +0000)

### Other

- - [USP][CDRouter][Random] Some datamodel path are missing in USP hl-api tests
- - [USP][CDRouter][Random] Some datamodel path are missing in USP hl-api tests

## Release v1.26.8 - 2024-09-10(07:26:32 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds

## Release v1.26.7 - 2024-08-02(15:02:06 +0000)

### Other

- add support for multiple runlevels

## Release v1.26.6 - 2024-07-29(06:08:27 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v1.26.5 - 2024-07-18(15:35:35 +0000)

### Other

- - [Prpl][CI][tr181-device] Plugins exits on start in reboot scenario (amxs synchronizaiton failed)

## Release v1.26.4 - 2024-06-12(15:07:05 +0000)

### Other

- - [dhcpv4-client] possible to add multiple enabled option with same tag

## Release v1.26.3 - 2024-04-10(10:05:06 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.26.2 - 2024-04-08(16:00:14 +0000)

### Fixes

- [DHCPv4Client] [API] Provide a Renew and Release Implementation

## Release v1.26.2 - 2024-04-08(14:35:50 +0000)

### Fixes

- [DHCPv4Client] [API] Provide a Renew and Release Implementation

## Release v1.26.1 - 2024-03-25(10:02:12 +0000)

### Fixes

- [tr181-dhcpv4client] setting interface in defaults odl cause crash

## Release v1.26.0 - 2024-03-23(13:08:24 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v1.25.5 - 2024-03-16(17:12:02 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v1.25.4 - 2024-03-04(12:02:12 +0000)

### Fixes

- [tr181-dhcpv4client] increase test coverage of dhcpv4client

## Release v1.25.3 - 2024-02-10(16:28:11 +0000)

### Fixes

- [AP Config]No DHCP option 125 sent by the repeater

## Release v1.25.2 - 2024-01-04(08:49:47 +0000)

### Fixes

- [DHCPv4Client] Lease Time Remaining value isn't updated after renew IP addr

## Release v1.25.1 - 2023-12-13(16:29:20 +0000)

### Fixes

- [Guest Bridge] Add guest support on AP, using ethernet backhaul.

## Release v1.25.0 - 2023-12-05(15:48:54 +0000)

### New

- [DHCPv4Client] [API] Provide a Renew and Release API's

## Release v1.24.3 - 2023-11-30(09:01:01 +0000)

### Other

- Set `lla` controllers by default

## Release v1.24.2 - 2023-11-27(14:18:02 +0000)

### Changes

- GetWANStatus not working after updating data using WANManager

## Release v1.24.1 - 2023-10-19(09:32:25 +0000)

### Other

- [AP Config] DHCP address should not immediately removed if the lan cable is unplugged

## Release v1.24.0 - 2023-10-17(13:10:38 +0000)

### New

- Issue: HOP-4523[Time] Handle the DHCP option 100 acording to timezones FiOS or FWA

## Release v1.23.1 - 2023-10-13(14:12:28 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v1.23.0 - 2023-10-09(12:11:39 +0000)

### New

- [DeviceInfo] Environment script cleanup and allow for modular environment definition

## Release v1.22.1 - 2023-10-09(06:58:05 +0000)

### Other

- [Time][DHCPOption] Add the functionality to configure the NTPServers based on the DHCP option

## Release v1.22.0 - 2023-09-07(15:22:02 +0000)

### New

- [amxrt][no-root-user][capability drop] [tr181-dhcpv4client] tr181-dhcpv4client plugin must be adapted to run as non-root and lmited capabilities

## Release v1.21.1 - 2023-08-03(11:35:26 +0000)

### Changes

- [DHCPv4][CWMP versus USP DM] Add Renew parameter

## Release v1.21.0 - 2023-07-06(08:07:19 +0000)

### New

- [TR181-DHCPv4Client] Implement statistics for mod-dhcpv4c

## Release v1.20.1 - 2023-06-30(12:18:32 +0000)

### Other

- [tr181-dhcpv4client] update tr181-dhcpv4client to use amx modules instead of amxb_call

## Release v1.20.0 - 2023-06-27(12:51:15 +0000)

### New

-  [TR181-DHCPv4Client] It must be possible to configure broadcast/unicast mode

## Release v1.19.0 - 2023-06-15(10:26:43 +0000)

### New

- Add DSCPMark and PriorityMark parameters

## Release v1.18.0 - 2023-06-13(13:51:53 +0000)

### New

- Add ReleaseOnReboot parameter

## Release v1.17.0 - 2023-06-07(08:37:36 +0000)

### New

- Add LastConnectionError parameter

## Release v1.16.1 - 2023-06-06(07:44:19 +0000)

## Release v1.16.0 - 2023-05-25(16:38:01 +0000)

### New

- Add authentication parameters

## Release v1.15.1 - 2023-05-17(12:32:04 +0000)

### Fixes

- Use the vendorid option for send option 60 in uci

## Release v1.15.0 - 2023-05-11(13:02:56 +0000)

### New

- Add default hostname and vendor class id for each dhcp interface

## Release v1.14.1 - 2023-05-11(09:03:13 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v1.14.0 - 2023-05-04(14:36:17 +0000)

### New

- Add retransmission configuration to the datamodel

### Fixes

- Treat Idle as Init state

## Release v1.13.0 - 2023-05-02(13:28:57 +0000)

### New

- [DHCPv4Client][option 121] Dynamically create routes based on option121

## Release v1.12.3 - 2023-04-27(09:31:39 +0000)

### Fixes

- Get the client ID info before starting the dhcp backend

## Release v1.12.2 - 2023-04-20(13:51:44 +0000)

### Fixes

- service protocol should be integers

## Release v1.12.1 - 2023-04-17(08:23:11 +0000)

### Fixes

- [odl]Remove deprecated odl keywords

## Release v1.12.0 - 2023-04-04(10:39:51 +0000)

### New

- Add mod-dhcpv4c as backend

## Release v1.11.0 - 2023-03-27(11:16:59 +0000)

### New

- [tr181-dhcpv4client] Status should stay Enabled when the interface goes down

## Release v1.10.2 - 2023-03-17(18:41:39 +0000)

### Other

- [baf] Correct typo in config option

## Release v1.10.1 - 2023-03-16(14:12:28 +0000)

### Other

- Add AP config files

## Release v1.10.0 - 2023-03-13(15:52:26 +0000)

### Fixes

- [DHCPv4] Split client and server plugin

### Other

- tr181-components: missing explicit dependency on rpcd service providing ubus uci backend

## Release v1.9.8 - 2023-02-24(15:45:43 +0000)

### Other

- Fix clashing installation of udhcpc.user script with netifd in openwrt22

## Release v1.9.7 - 2023-02-23(12:39:45 +0000)

### Fixes

- Resolve name conflict between main plugin module and new backend module

## Release v1.9.6 - 2023-02-13(15:38:21 +0000)

## Release v1.9.5 - 2023-02-07(12:39:01 +0000)

### Fixes

- [CDROUTER][DHCP] Box is no more sending Hostname DHCP Option 12 in WAN DHCP packets

## Release v1.9.4 - 2023-02-06(19:45:53 +0000)

### Fixes

- [DHCPv4 Renew] typo in Renew() command

## Release v1.9.3 - 2023-02-03(15:31:47 +0000)

### Fixes

- When DHCP Client Option 60 is set using data model, the default DHCP option 60 is not modified and 2 DHCP option60 are sent

## Release v1.9.2 - 2023-01-27(11:12:48 +0000)

### Fixes

- debug logs in syslog with trace level 200

## Release v1.9.1 - 2023-01-09(10:15:23 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v1.9.0 - 2023-01-05(11:52:39 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v1.8.2 - 2022-12-16(12:28:56 +0000)

### Fixes

-  [WANMode] Add default WANModes (Ethernet_DHCP and Ethernet_PPP)

## Release v1.8.1 - 2022-12-15(15:00:19 +0000)

### Fixes

- [WANMode] Add default WANModes (Ethernet_DHCP and Ethernet_PPP)

## Release v1.8.0 - 2022-12-14(08:18:30 +0000)

### New

- [WANMode] Add default WANModes (Ethernet_DHCP and Ethernet_PPP)

## Release v1.7.11 - 2022-12-13(13:21:58 +0000)

### Fixes

- Fails to get an address at firstboot when default wan mode is tagged

## Release v1.7.10 - 2022-12-09(09:20:37 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v1.7.9 - 2022-11-07(12:17:34 +0000)

### Fixes

- gcc 11.2.0 linker cannot find -lsahtrace

## Release v1.7.8 - 2022-07-29(07:37:21 +0000)

### Fixes

- dhcpv servers fails to create cpe-dhcpvXs-* firewall services on firstboot

## Release v1.7.7 - 2022-07-28(12:07:04 +0000)

### Fixes

- DHCP v4/v6 managers started alongside clients

## Release v1.7.6 - 2022-07-27(13:47:54 +0000)

### Fixes

- dhcpv4 client is stuck in DHCPStatus==Init

## Release v1.7.5 - 2022-07-12(10:10:17 +0000)

## Release v1.7.4 - 2022-06-22(17:28:53 +0000)

### Fixes

- Datamodel does not load at startup

## Release v1.7.3 - 2022-06-14(10:49:34 +0000)

### Fixes

- [TR181 DHCPv4Client] Change Status parameter after DM is filled in

## Release v1.7.2 - 2022-05-12(11:16:44 +0000)

### Fixes

- [TR181 DHCPv4Client] kill previously launched udhcpc when fnc dhcpv4c-start is called

## Release v1.7.1 - 2022-05-12(06:43:22 +0000)

### Fixes

- [TR181 DHCPv4client]Set Enabled as Status in datamodel only if parameters are filled in

## Release v1.7.0 - 2022-05-04(15:15:50 +0000)

### New

- direct way implementation (without uci)

## Release v1.6.12 - 2022-04-28(07:57:34 +0000)

### Changes

- udhcpc should call the tr181-dhcpv4client script

## Release v1.6.11 - 2022-04-14(11:24:45 +0000)

### Other

- Remove the calls to IP-manager to set and remove IP

## Release v1.6.10 - 2022-04-14(07:27:35 +0000)

### Changes

- Make the Interface path prefixed

## Release v1.6.9 - 2022-04-05(06:30:55 +0000)

### Fixes

- [TR181 DHCPv4Client] Fix DHCP client

## Release v1.6.8 - 2022-03-29(15:21:56 +0000)

### Fixes

- [TR181 DHCPv4Client] Config update failed

## Release v1.6.7 - 2022-03-24(10:47:04 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.6.6 - 2022-03-18(16:54:41 +0000)

### Fixes

- Crash at boot detected

## Release v1.6.5 - 2022-03-17(15:42:37 +0000)

### Fixes

- DHCP Option 61 Support

## Release v1.6.4 - 2022-03-14(16:41:05 +0000)

### Fixes

- After de ethernet cable is removed/interface goes down,...

## Release v1.6.3 - 2022-03-11(17:18:22 +0000)

### Other

- Use NetModel to listen on Interface changes

## Release v1.6.2 - 2022-02-25(11:53:23 +0000)

### Other

- Enable core dumps by default

## Release v1.6.1 - 2022-02-09(12:54:21 +0000)

### Fixes

- daemon sometimes segfaults during startup

## Release v1.6.0 - 2022-01-11(14:29:32 +0000)

### New

- Create DHCPv4 Client mapping in NetModel

## Release v1.5.0 - 2021-12-21(17:39:34 +0000)

### New

- retrieve requested options from dhcpv4 client (udhcpc)

## Release v1.4.2 - 2021-11-30(15:00:00 +0000)

### Other

- [CI] Add missing dependency for libdhcpoptions

## Release v1.4.1 - 2021-11-30(13:34:27 +0000)

### Fixes

- update changes of ReqOption/SentOption to uci

## Release v1.4.0 - 2021-11-29(16:26:43 +0000)

### New

- Update the ipv4 instances in ip-manager

## Release v1.3.6 - 2021-11-23(08:17:09 +0000)

### Changes

- Use TR181 path in parameter Interface of datamodel

## Release v1.3.5 - 2021-10-26(10:54:57 +0000)

### Fixes

- Generate event when changing Status, ..., in datamodel

## Release v1.3.4 - 2021-10-25(13:36:45 +0000)

### Fixes

- use common datamodel with DHCPv4 server

## Release v1.3.3 - 2021-10-11(08:17:23 +0000)

### Fixes

- tr181 DHCP Client set start up order to 27

## Release v1.3.2 - 2021-10-05(11:56:33 +0000)

### Fixes

- Building modules in unit test

## Release v1.3.1 - 2021-09-17(14:40:45 +0000)

### Fixes

-  Firewall Support

### Other

- Opensource component

## Release v1.3.0 - 2021-09-12(07:02:25 +0000)

### New

- Use uci to enable(and start) and disable(stop) udhcpc

## Release v1.2.0 - 2021-09-09(11:39:23 +0000)

### New

- LeaseTimeRemaining

## Release v1.1.0 - 2021-09-03(11:39:34 +0000)

### New

- udhcpc usage of SIGUSR1 & 2, firewall open/close udp port 68

## Release v1.0.4 - 2021-08-24(16:38:55 +0000)

### Changes

- Datamodel updated by dhcp.script

## Release v1.0.3 - 2021-08-13(11:41:54 +0000)

### Changes

- Add changelog.md
- Unit test added

