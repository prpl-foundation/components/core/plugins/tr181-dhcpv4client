/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <errno.h>
#include <fcntl.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxb/amxb_types.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "mod-udhcpc-access.h"
#include "udhcpc.h"
#include "../common/mock.h"
#include "../common/mock_functions.h"
#include "test_module.h"

#define SCRIPT_START            "../common/data/data_start_dhcpv4c-1.json"
#define SCRIPT_DATA_DECONFIG    "../common/data/script_data_deconfig.json"
#define SCRIPT_DATA_BOUND       "../common/data/script_data_bound.json"
#define SCRIPT_DATA_RENEW       "../common/data/script_data_renew.json"

amxp_subproc_t* current_subproc = NULL;

int __wrap_amxp_subproc_astart(amxp_subproc_t* const subproc,
                               amxc_array_t* cmd);
int __wrap_amxp_subproc_get_exitstatus(amxp_subproc_t* subproc);
int __wrap_amxp_subproc_kill(const amxp_subproc_t* const subproc,
                             const int sig);
int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);
int __real_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);
int __wrap_kill(pid_t pid, int sig);

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int __wrap_amxp_subproc_astart(amxp_subproc_t* const subproc,
                               amxc_array_t* array) {
    static int stop_signal = 0;
    int index = 0;
    char* arg = "";
    const char* cmd[] = {
        "udhcpc",
        "-S",
        "-s",
        "/etc/amx/tr181-dhcpv4client/udhcpc-script.sh",
        "-f",
        "-t",
        "0",
        "-i",
        "eth0",
        NULL
    };
    while(arg != NULL) {
        arg = (char*) amxc_array_get_data_at(array, index);
        if((arg == NULL) || (cmd[index] == NULL)) {
            break;
        }
        assert_string_equal(cmd[index], arg);
        index++;
    }
    stop_signal++;
    /*
     * First test of starting dhcpv4 client simulates a failure
     * See also
     * __wrap_amxp_subproc_get_exitstatus
     * test_dhcpv4c_start_misconfigured
     */
    if(stop_signal == 1) {
        amxp_sigmngr_emit_signal(amxp_subproc_get_sigmngr(subproc), "stop", NULL);
    }
    current_subproc = subproc;
    return 0;
}

int __wrap_amxp_subproc_get_exitstatus(UNUSED amxp_subproc_t* subproc) {
    static int retval = 0;
    retval = !retval;
    return retval;
}

int __wrap_amxp_subproc_kill(UNUSED const amxp_subproc_t* const subproc,
                             UNUSED const int sig) {
    return 0;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    static int nr_fnc_called = 0;
    const char* exp_mod_name = "mod-tr181-dhcpv4c";
    char* str_args = amxc_var_dyncast(cstring_t, args);

    assert_string_equal(exp_mod_name, module_name);
    if(strcmp(func_name, "update-client") == 0) {
        amxc_var_t* params = GET_ARG(args, "parameters");
        assert_non_null(params);

        // Check presence of parameters:
        // ifname, bound
        const char* ifname = GET_CHAR(params, "ifname");
        const char* dns = GET_CHAR(params, "DNSServers");
        const char* ip = GET_CHAR(params, "IPAddress");
        const char* router = GET_CHAR(params, "IPRouters");
        const char* lease = GET_CHAR(params, "LeaseTimeRemaining");
        const char* subnet = GET_CHAR(params, "SubnetMask");
        const bool bound = GET_BOOL(params, "bound");
        assert_non_null(ifname);
        if(++nr_fnc_called == 1) {
            assert_string_equal(ifname, "eth0");
            assert_string_equal(dns, "192.168.0.250,192.168.0.251");
            assert_string_equal(ip, "192.168.0.10");
            assert_string_equal(router, "192.168.0.1");
            assert_string_equal(lease, "300000");
            assert_string_equal(subnet, "255.255.255.0");
            assert_true(bound);
        } else if(nr_fnc_called == 2) {
            assert_string_equal(ifname, "eth0");
            assert_string_equal(dns, "192.168.0.250,192.168.0.251");
            assert_string_equal(ip, "192.168.0.10");
            assert_string_equal(router, "192.168.0.1");
            assert_string_equal(lease, "123321");
            assert_string_equal(subnet, "255.255.255.0");
            assert_true(bound);
        } else if(nr_fnc_called == 3) {
            assert_string_equal(ifname, "eth0");
            assert_false(bound);
        } else {
            // Assert here
            // amxm_execute_function with function server-add is only called 2 times
            assert_int_equal(nr_fnc_called, 3);
        }
    } else if(strcmp(func_name, "client-misconfigured") == 0) {
        const char* exp_args = "Interface:Device.IP.Interface.2.";
        assert_string_equal(exp_args, str_args);
    } else if(strcmp(func_name, "client-stopped") == 0) {
        const char* exp_args = "Interface:Device.IP.Interface.2.";
        assert_string_equal(exp_args, str_args);
    } else {
        printf("mocking amxm_execute_function: %s %s\n",
               module_name, func_name);
        printf("args: '%s'\n", str_args);
        fflush(stdout);
        amxc_var_dump(args, STDOUT_FILENO);
        // Assert here
        assert_string_equal(func_name, " unknown-function ");
    }
    free(str_args);
    return 0;
}

int __wrap_kill(pid_t pid, UNUSED int sig) {
    static int nr_fnc_called = 0;
    const int sig_nrs[] = { SIGUSR1, SIGUSR2, SIGTERM, 0, SIGKILL, SIGKILL };
    nr_fnc_called++;

    // kill is only called 6 times
    assert_false(nr_fnc_called > 6);
    assert_int_equal(1234, pid);
    assert_int_equal(sig_nrs[nr_fnc_called - 1], sig);
    if(nr_fnc_called == 3) {
        // if following line asserts, nr_fnc_called is wrongly specified
        assert_int_equal(sig, SIGTERM);
        // Only for first SIGTERM, send a stop signal
        amxp_sigmngr_emit_signal(amxp_subproc_get_sigmngr(current_subproc), "stop", NULL);
    }
    return 0;
}

int test_setup(UNUSED void** state) {
    return 0;
}

int test_teardown(UNUSED void** state) {
    handle_events();
    return 0;
}

void test_dummy_fnc(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    assert_int_equal(dummy_func("dummy_func", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    handle_events();
}

void test_dhcpv4c_stop_error_1(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    assert_int_equal(dhcpv4c_set_state_stop("dhcpv4c_stop", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), -1);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    handle_events();
}

void test_dhcpv4c_stop_error_2(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");

    assert_int_equal(dhcpv4c_set_state_stop("dhcpv4c_stop", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), -1);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    handle_events();
}

void test_dhcpv4c_stop_error_3(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(cstring_t, &data, "IfName", "eth0");

    assert_int_equal(dhcpv4c_set_state_stop("dhcpv4c_stop", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), -1);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    handle_events();
}

void test_dhcpv4c_start_error_1(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    assert_int_equal(dhcpv4c_set_state_start("dhcpv4c_start", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), -1);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    handle_events();
}

/*
 * Exported function dhcpv4c_start will return 0, but we simulate here that:
 * udhcpc component immediately ends with a non-zero return code
 * module under test must call function client-misconfigured of the core plugin
 */
void test_dhcpv4c_start_misconfigured(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(cstring_t, &data, "IfName", "eth0");

    assert_int_equal(dhcpv4c_set_state_start("dhcpv4c_start", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    handle_events();
}

void test_dhcpv4c_start(UNUSED void** state) {
    amxc_var_t* data;
    amxc_var_t ret;

    createfile("1234/n");

    amxc_var_init(&ret);
    data = dummy_read_json_from_file(SCRIPT_START);

    assert_int_equal(dhcpv4c_set_state_start("dhcpv4c_start", data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);
    amxc_var_clean(&ret);
    amxc_var_delete(&data);

    handle_events();
}

static void test_user_script_udhcpc(const char* phys_interf,
                                    const char* data_args,
                                    const char* retval) {
    amxc_var_t* data = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    if(data_args != NULL) {
        data = dummy_read_json_from_file(data_args);
    }
    if(data == NULL) {
        amxc_var_new(&data);
        amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    }

    if(phys_interf != NULL) {
        amxc_var_add_key(cstring_t, data, "interface", phys_interf);
    }
    assert_int_equal(udhcpc_user_script("udhcpc_user_script", data, &ret), 0);
    const char* str_ret = amxc_var_constcast(cstring_t, &ret);
    if(str_ret) {
        assert_string_equal(str_ret, retval);
    } else {
        printf("retval=%s\n", retval);
        assert_null(retval);
    }
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
    handle_events();
}

void test_user_script_bound(UNUSED void** state) {
    test_user_script_udhcpc("eth0", SCRIPT_DATA_BOUND, NULL);
}

void test_dhcpv4c_renew(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    createfile("1234/n");

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set(cstring_t, &data, "eth0");

    assert_int_equal(dhcpv4c_renew("dhcpv4c_renew", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    handle_events();
}

void test_user_script_renew(UNUSED void** state) {
    test_user_script_udhcpc("eth0", SCRIPT_DATA_RENEW, NULL);
}

void test_dhcpv4c_release(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    createfile("1234/n");

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set(cstring_t, &data, "eth0");

    assert_int_equal(dhcpv4c_release("dhcpv4c_release", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    handle_events();
}

void test_user_script_deconfig(UNUSED void** state) {
    test_user_script_udhcpc("eth0", SCRIPT_DATA_DECONFIG, NULL);
}

void test_dhcpv4c_stop(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(cstring_t, &data, "IfName", "eth0");

    assert_int_equal(dhcpv4c_set_state_stop("dhcpv4c_stop", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);

    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    handle_events();
}
