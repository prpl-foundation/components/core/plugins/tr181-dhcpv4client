/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxb/amxb.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>

#include <amxm/amxm.h>

#include "v4v6option.h"
#include "mod-udhcpc-access.h"
#include "udhcpc.h"
#include "udhcpc_list.h"

static int udhcpc_stop(udhcpc_t* odhcpc);
static void udhcpc_stop_handler(const char* const sig_name,
                                const amxc_var_t* const data,
                                void* const priv);
void update_state(udhcpc_t* udhcpc);

static void args_array_it_free(amxc_array_it_t* it) {
    free(it->data);
}

static int get_option_tag(char* str_opt) {
    int rv = 0xFF;
    char* tag;

    when_str_empty_trace(str_opt, exit, WARNING, "Empty string option");
    tag = strtok(str_opt, ":");
    rv = (int) strtol(tag, NULL, 0);
exit:
    return rv;
}

static unsigned char* get_option_value(char* str_opt) {
    unsigned char* value = NULL;
    char* pos = NULL;
    uint32_t length = 0;

    when_str_empty_trace(str_opt, exit, ERROR, "Empty string option");
    pos = strchr(str_opt, (int) ':');
    when_null_trace(pos, exit, ERROR, "The SentOption string is malformed.");
    value = dhcpoption_option_convert2bin(pos + 1, &length);
    SAH_TRACEZ_INFO(ME, "OUTPUT STRING %s", value);
exit:
    return value;
}

// udhcpc -S -p /var/run/udhcpc-eth0.pid -s /lib/netifd/dhcp.script -f -t 0 -i eth0 -x hostname:OpenWrt -x 0x3d:01021018010C01 -R -O 1 -O 3 -O 6 -O 15 -O 43 -O 51 -O 58 -O 59 -O 212 -O 121
static int udhcpc_start_process(udhcpc_t* udhcpc,
                                amxc_array_t* req_opts,
                                amxc_array_t* sent_opts) {
    int retval = -1;
    uint32_t index = 0;
    size_t nr_req_opts = amxc_array_size(req_opts);   // returns 0 if ptr is NULL
    size_t nr_sent_opts = amxc_array_size(sent_opts); // returns 0 if ptr is NULL
    size_t nr_args = 11 + 2 * nr_sent_opts + 2 * nr_req_opts;
    amxc_array_t cmd;
    const char* ifname = NULL;
    char* str_opt = NULL;
    unsigned char* opt_value = NULL;

    when_null_trace(udhcpc, exit, ERROR, "Bad odhcp input value");
    when_null_trace(udhcpc->subproc, exit, ERROR, "Could not find the odhcp subproc");
    when_false_trace(!amxp_subproc_is_running(udhcpc->subproc), exit, ERROR, "trying to start the subproc, but the subproc is already running");
    when_null_trace(udhcpc->used_args, exit, ERROR, "Could not find the odhcp used_args parameter");

    ifname = GET_CHAR(udhcpc->used_args, "IfName");
    when_null_trace(ifname, exit, ERROR, "Could not find the odhcp ifname");

    amxc_array_init(&cmd, nr_args);
    amxc_array_append_data(&cmd, strdup("udhcpc"));
    amxc_array_append_data(&cmd, strdup("-S"));
    amxc_array_append_data(&cmd, strdup("-s"));
    amxc_array_append_data(&cmd, strdup("/etc/amx/tr181-dhcpv4client/udhcpc-script.sh"));
    amxc_array_append_data(&cmd, strdup("-f"));
    amxc_array_append_data(&cmd, strdup("-t"));
    amxc_array_append_data(&cmd, strdup("0"));
    amxc_array_append_data(&cmd, strdup("-i"));
    amxc_array_append_data(&cmd, strdup(ifname));
    while(nr_req_opts > 0) {
        amxc_array_append_data(&cmd, strdup("-O"));
        amxc_array_append_data(&cmd, strdup(amxc_array_get_data_at(req_opts, index)));
        index++;
        nr_req_opts--;
    }
    amxc_array_append_data(&cmd, strdup("-R"));
    index = 0;
    while(nr_sent_opts > 0) {
        str_opt = strdup(amxc_array_get_data_at(sent_opts, index));

        switch(get_option_tag(str_opt)) {
        case 0x3C:
            // Tag 60: Vendor Class Identifier
            opt_value = get_option_value(amxc_array_get_data_at(sent_opts, index));
            amxc_array_append_data(&cmd, strdup("-V"));
            amxc_array_append_data(&cmd, opt_value);
            break;
        case 0xFF:
            SAH_TRACE_WARNING("Unable to retrieve Tag from SentOption string.");
            break;
        default:
            amxc_array_append_data(&cmd, strdup("-x"));
            amxc_array_append_data(&cmd, strdup(amxc_array_get_data_at(sent_opts, index)));
            break;
        }
        index++;
        nr_sent_opts--;
        free(str_opt);
    }
    retval = amxp_subproc_astart(udhcpc->subproc, &cmd);
    when_false_trace(retval == 0, exit, ERROR, "Failed to start the subproc");
    SAH_TRACEZ_INFO(ME, "amxp_subproc_astart (udhcpc %s, pid %d) ret %d", ifname, udhcpc->subproc->pid, retval);
    amxc_array_clean(&cmd, args_array_it_free);
exit:
    return retval;
}

static void udhcpc_set_sent_options(amxc_var_t* opts,
                                    amxc_array_t* array) {
    size_t size = 0;
    amxc_string_t str_opts;

    amxc_string_init(&str_opts, 0);
    when_null_trace(opts, exit, INFO, "No SentOption defined");
    when_null_trace(array, exit, ERROR, "No array defined");

    amxc_var_for_each(var, opts) {
        size++;
    }
    amxc_array_grow(array, size);

    amxc_var_for_each(var, opts) {
        char* data = NULL;
        uint8_t tag = (uint8_t) GET_UINT32(var, "Tag");
        const char* opt_value = GET_CHAR(var, "Value");
        if((GET_BOOL(var, "Enable") == false) || str_empty(opt_value)) {
            continue;
        }
        amxc_string_setf(&str_opts, "0x%02X:%s", tag, opt_value);
        data = amxc_string_take_buffer(&str_opts);
        // memory of 'data'' is freed when cleaning up array
        amxc_array_append_data(array, (void*) data);
    }
exit:
    amxc_string_clean(&str_opts);
    return;
}

static void udhcpc_set_request_options(amxc_var_t* opts,
                                       amxc_array_t* array) {
    size_t size = 0;
    amxc_string_t str_opts;

    amxc_string_init(&str_opts, 0);
    when_null_trace(opts, exit, INFO, "No ReqOption defined");

    amxc_var_for_each(var, opts) {
        size++;
    }
    amxc_array_grow(array, size);

    amxc_var_for_each(var, opts) {
        char* data = NULL;
        uint8_t tag = (uint8_t) GET_UINT32(var, "Tag");
        if(GET_BOOL(var, "Enable") == true) {
            amxc_string_setf(&str_opts, "%hhu", tag);
            data = amxc_string_take_buffer(&str_opts);
            // memory of 'data'' is freed when cleaning up array
            amxc_array_append_data(array, (void*) data);
        }
    }
exit:
    amxc_string_clean(&str_opts);
    return;
}

static void udhcpc_free_array_item(amxc_array_it_t* it) {
    free(it->data);
}

static int udhcpc_start(udhcpc_t* udhcpc, amxc_var_t* args) {
    int retval = -1;
    amxc_array_t req_opts;
    amxc_array_t sent_opts;

    amxc_array_init(&req_opts, 0);
    amxc_array_init(&sent_opts, 0);

    when_null_trace(udhcpc, exit, ERROR, "Invalid udhcpc input parameter");
    when_null_trace(args, exit, ERROR, "Invalid args input parameter");

    udhcpc->state = ODHCP_STARTING;
    retval = amxp_slot_connect(amxp_subproc_get_sigmngr(udhcpc->subproc), "stop", NULL, udhcpc_stop_handler, (void*) udhcpc);
    when_false_trace(retval == 0, exit, ERROR, "Failed to connect stop handler");
    udhcpc_set_sent_options(GETP_ARG(args, "SentOption"), &sent_opts);
    udhcpc_set_request_options(GETP_ARG(args, "ReqOption"), &req_opts);
    retval = udhcpc_start_process(udhcpc, &req_opts, &sent_opts);
    when_false_trace(retval == 0, exit, ERROR, "Failed to execute odhcp6c_start_process");
exit:
    amxc_array_clean(&req_opts, udhcpc_free_array_item);
    amxc_array_clean(&sent_opts, udhcpc_free_array_item);
    return retval;
}

void dhcpv4c_client_stopped(const char* interface) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_str_empty_trace(interface, exit, ERROR, "Interface path missing");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", interface);
    retval = amxm_execute_function(NULL, MOD_CORE, "client-stopped", &data, &ret);
    when_false_trace(retval == 0, exit, ERROR, "Failed to execute amxm_execute_function client-stopped");
    SAH_TRACEZ_INFO(ME, "Fnc \"client-stopped\" returned %d", retval);
exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

static void udhcpc_misconfigured(const char* interface) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", interface);

    retval = amxm_execute_function(NULL, MOD_CORE, "client-misconfigured", &data, &ret);
    when_false_trace(retval == 0, exit, ERROR, "Failed to execute amxm_execute_function client-misconfigured");
    SAH_TRACEZ_INFO(ME, "Fnc \"client-misconfigured\" returned %d", retval);
exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

static void udhcpc_stop_handler(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                void* const priv) {
    udhcpc_t* udhcpc = (udhcpc_t*) priv;
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "The stop odhcp handler has been triggered");

    when_null_trace(udhcpc, exit, ERROR, "Could not find the odhcp struct in the priv data");
    when_null_trace(udhcpc->interface, exit, ERROR, "Could not find the odhcp interface");

    retval = amxp_slot_disconnect(amxp_subproc_get_sigmngr(udhcpc->subproc), "stop", udhcpc_stop_handler);
    when_false_trace(retval == 0, exit, ERROR, "Failed to disconnect stop handler");

    if(GET_INT32(data, "ExitCode") != 0) {
        SAH_TRACEZ_ERROR(ME, "ODHCP closed with an exit error %d", GET_INT32(data, "ExitCode"));
        udhcpc_misconfigured(udhcpc->interface);
    } else {
        dhcpv4c_client_stopped(udhcpc->interface);
    }
    udhcpc->state = ODHCP_STOPPED;
    update_state(udhcpc);
exit:
    return;
}

static void udhcpc_cleanup_process_cb(udhcpc_t* udhcpc) {
    amxc_var_t data_var;
    amxc_var_t ret;
    int retval = -1;

    amxc_var_init(&data_var);
    amxc_var_init(&ret);

    when_null_trace(udhcpc, exit, ERROR, "Bad odhcp input parameter");
    when_null_trace(udhcpc->subproc, exit, ERROR, "Could not find the odhcp subproc");
    when_false(amxp_subproc_is_running(udhcpc->subproc), exit);

    retval = amxp_slot_disconnect(amxp_subproc_get_sigmngr(udhcpc->subproc), "stop", udhcpc_stop_handler);
    when_false_trace(retval == 0, exit, ERROR, "Failed to disconnect stop handler");

    udhcpc_stop(udhcpc);
    amxc_var_clean(udhcpc->used_args);

    amxc_var_set_type(&data_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int32_t, &data_var, "ExitCode", 0);
    udhcpc_stop_handler(NULL, &data_var, (void*) udhcpc);
exit:
    amxc_var_clean(&data_var);
    amxc_var_clean(&ret);
}

static int udhcpc_stop(udhcpc_t* udhcpc) {
    int retval = -1;

    when_null_trace(udhcpc, exit, ERROR, "Bad odhcp input parameter");
    when_null_trace(udhcpc->subproc, exit, ERROR, "Could not find the odhcp subproc");
    when_false_trace(amxp_subproc_is_running(udhcpc->subproc), exit, ERROR, "trying to stop the subproc, but the subproc is not running");

    udhcpc->state = ODHCP_STOPPING;

    retval = amxp_subproc_kill(udhcpc->subproc, SIGTERM);
    when_false_trace(retval == 0, exit, ERROR, "Failed to kill the subproc with SIGTERM %s (%d)", strerror(errno), errno);
    SAH_TRACEZ_INFO(ME, "Terminate udhcpc[%d]", udhcpc->subproc->pid);
exit:
    return retval;
}

void update_state(udhcpc_t* udhcpc) {
    when_null_trace(udhcpc, exit, ERROR, "Bad udhcpc input parameter");

    if((udhcpc->state == ODHCP_STARTED) || (udhcpc->state == ODHCP_STARTING) || (udhcpc->state == ODHCP_STOPPED)) {
        if((amxc_var_get_first(udhcpc->used_args) != NULL) && (amxc_var_get_first(udhcpc->latest_args) != NULL)) {
            int cmp = -1;
            int res = amxc_var_compare(udhcpc->used_args, udhcpc->latest_args, &cmp);
            when_failed_trace(res, exit, ERROR, "Failed to compare two variants");
            when_false_trace(cmp != 0, exit, INFO, "Changed to the correct state %d", udhcpc->state);
        }

        //Check if the process is in process
        if(amxc_var_get_first(udhcpc->latest_args) == NULL) {
            //Should be stopped
            udhcpc_stop(udhcpc);
            amxc_var_clean(udhcpc->used_args);
        } else {
            //Should be started
            if(amxc_var_get_first(udhcpc->used_args) == NULL) {
                amxc_var_copy(udhcpc->used_args, udhcpc->latest_args);
                udhcpc_start(udhcpc, udhcpc->used_args);
            } else {
                //Started, but not with the most recent arguments
                udhcpc_stop(udhcpc);
                amxc_var_clean(udhcpc->used_args);
            }
        }
    }
exit:
    return;
}

/**
   @brief
   Start a DHCPv4 client

   @param function_name function name
   @param args { Interface = "IP.Interface.2", ifname = "eth0",
                 SentOption [ {Tag = 111,  Value = "" },...],
                 RequestedOptions = "" }
   @return Always 0
 */
int dhcpv4c_set_state_start(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    udhcpc_t* udhcpc = NULL;
    const char* interface = NULL;
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Got a signal to start the udhcp process");

    when_null_trace(args, exit, ERROR, "Bad args input parameter given");
    interface = GET_CHAR(args, "Interface");
    when_str_empty_trace(interface, exit, ERROR, "bad input value 'interface'");

    udhcpc = udhcpc_list_find(interface);
    if(udhcpc == NULL) {
        udhcpc = udhcpc_list_add(args);
    }
    when_null_trace(udhcpc, exit, ERROR, "Failed to create a udhcpc info struct");
    amxc_var_copy(udhcpc->latest_args, args);
    update_state(udhcpc);
    retval = 0;
exit:
    amxc_var_set(int32_t, ret, retval);
    return 0;
}

/**
   @brief
   Stop DHCPv4 client

   @param function_name function name
   @param args { Interface = "IP.Interface.2." }
   @param ret status value, 0 if ok
   @return 0 if success
 */
int dhcpv4c_set_state_stop(UNUSED const char* function_name,
                           amxc_var_t* args,
                           amxc_var_t* ret) {
    const char* interface = NULL;
    udhcpc_t* udhcpc = NULL;
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Got a signal to stop the udhcp process");

    when_null_trace(args, exit, ERROR, "Bad args input parameter given");
    interface = GET_CHAR(args, "Interface");
    when_str_empty_trace(interface, exit, ERROR, "Bad input value 'interface'");

    udhcpc = udhcpc_list_find(interface);
    when_null_trace(udhcpc, exit, ERROR, "Could not find the udhcp struct for the interface %s", interface);
    amxc_var_clean(udhcpc->latest_args);
    update_state(udhcpc);
    retval = 0;
exit:
    amxc_var_set(int32_t, ret, retval);
    return 0;
}


int dummy_func(UNUSED const char* function_name,
               UNUSED amxc_var_t* args,
               amxc_var_t* ret) {
    amxc_var_set(int32_t, ret, 0);
    return 0;
}

static AMXM_CONSTRUCTOR mod_udhcpc_access_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "mod-udhcpc-access %s", "start");

    amxm_module_register(&mod, so, MOD_NAME);
    amxm_module_add_function(mod, "dhcpv4c-start", dhcpv4c_set_state_start);
    amxm_module_add_function(mod, "dhcpv4c-stop", dhcpv4c_set_state_stop);
    amxm_module_add_function(mod, "client-change", dummy_func);
    amxm_module_add_function(mod, "update-config", dummy_func);
    amxm_module_add_function(mod, "dhcpv4c-renew", dhcpv4c_renew);
    amxm_module_add_function(mod, "dhcpv4c-release", dhcpv4c_release);
    amxm_module_add_function(mod, "user-script", udhcpc_user_script);
    amxm_module_add_function(mod, "dhcpv4c-config-update", dummy_func);
    amxm_module_add_function(mod, "raw-sock-handler", dummy_func);
    amxm_module_add_function(mod, "dhcpv4c-get-statistics", dummy_func);
    amxm_module_add_function(mod, "dhcpv4c-clear-statistics", dummy_func);

    udhcpc_list_init();
    return 0;
}

static AMXM_DESTRUCTOR mod_udhcpc_access_stop(void) {
    SAH_TRACEZ_INFO(ME, "mod-udhcpc-access %s", "stop");

    amxp_timers_enable(false);
    udhcpc_list_all(udhcpc_cleanup_process_cb);
    udhcpc_list_delete_all();
    return 0;
}
