/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "udhcpc_list.h"

extern amxc_llist_t* if_list;

void udhcpc_list_init(void) {
    if(if_list == NULL) {
        amxc_llist_new(&if_list);
    }
}

static void udhcpc_list_delete_item(amxc_llist_it_t* it) {
    udhcpc_t* udhcpc = amxc_container_of(it, udhcpc_t, it);
    amxc_llist_it_take(it);

    amxc_var_delete(&(udhcpc->latest_args));
    amxc_var_delete(&(udhcpc->used_args));
    amxp_subproc_delete(&(udhcpc->subproc));

    free(udhcpc->interface);
    udhcpc->interface = NULL;
    free(udhcpc);
    udhcpc = NULL;
}

udhcpc_t* udhcpc_list_find(const char* interf) {
    udhcpc_t* ret_ptr = NULL;
    amxc_llist_for_each(it, if_list) {
        udhcpc_t* udhcpc = amxc_container_of(it, udhcpc_t, it);
        if(strcmp(interf, udhcpc->interface) == 0) {
            ret_ptr = udhcpc;
            break;
        }
    }
    return ret_ptr;
}

int udhcpc_list_delete(const char* interf) {
    int retval = -1;

    amxc_llist_for_each(it, if_list) {
        udhcpc_t* udhcpc = amxc_container_of(it, udhcpc_t, it);
        if(strcmp(interf, udhcpc->interface) == 0) {
            udhcpc_list_delete_item(it);
            retval = 0;
            break;
        }
    }
    return retval;
}

void udhcpc_list_all(udhcpc_cb_fnc_t func) {
    when_null(func, exit);
    amxc_llist_for_each(it, if_list) {
        func(amxc_container_of(it, udhcpc_t, it));
    }
exit:
    return;
}

udhcpc_t* udhcpc_list_add(amxc_var_t* args) {
    udhcpc_t* udhcpc = NULL;
    const char* interface = NULL;

    when_null_trace(args, exit, ERROR, "The given args parameter is invalid");
    interface = GET_CHAR(args, "Interface");
    when_null_trace(interface, exit, ERROR, "bad input interface value");
    when_false_trace(udhcpc_list_find(interface) == NULL, exit, ERROR, "their is already a odhcp struct in the list");

    udhcpc = (udhcpc_t*) calloc(1, sizeof(udhcpc_t));
    when_null_trace(udhcpc, exit, ERROR, "Failed to execute calloc");

    udhcpc->interface = strdup(interface);
    amxc_var_new(&udhcpc->latest_args);
    amxc_var_new(&udhcpc->used_args);
    amxc_llist_append(if_list, &(udhcpc->it));
    amxp_subproc_new(&udhcpc->subproc);
    udhcpc->state = ODHCP_STOPPED;

exit:
    return udhcpc;
}

void udhcpc_list_delete_all(void) {
    amxc_llist_delete(&if_list, udhcpc_list_delete_item);
    if_list = NULL;
}
