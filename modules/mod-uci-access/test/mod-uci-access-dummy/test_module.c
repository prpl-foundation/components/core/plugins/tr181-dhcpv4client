/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <errno.h>
#include <fcntl.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxb/amxb_types.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "test_module.h"

#define UCI_COMMIT              "../common/data/retval_0.json"
#define DATA_CHANGE_CLIENT      "../common/data/data_change-client.json"
#define DATA_CHANGE_CLIENT_2    "../common/data/data_change-client-2.json"

static amxd_dm_t dm;
static amxo_parser_t parser;
static int amxb_call_set_cnt = 0;
static int amxb_call_delete_cnt = 0;

amxc_var_t* dummy_read_json_from_file(const char* fname);

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout);

amxc_var_t* dummy_read_json_from_file(const char* fname) {
    int fd = -1;
    variant_json_t* reader = NULL;
    amxc_var_t* data = NULL;
    // create a json reader
    if(amxj_reader_new(&reader) != 0) {
        printf("Failed to create json file reader");
        goto exit;
    }

    // open the json file
    fd = open(fname, O_RDONLY);
    if(fd == -1) {
        printf("File open file %s - error 0x%8.8X", fname, errno);
        goto exit;
    }

    // read the json file and parse the json text
    amxj_read(reader, fd);

    // get the variant
    data = amxj_reader_result(reader);

    if(data == NULL) {
        printf("Invalid JSON in file %s", fname);
    }

    close(fd);
exit:
    // delete the reader and close the file
    if(reader) {
        amxj_reader_delete(&reader);
    }

    return data;
}

int __wrap_amxb_call(UNUSED amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     UNUSED int timeout) {
    assert_string_equal(object, "uci.");
    assert_string_equal(GET_CHAR(args, "config"), "network");

    if(strcmp(method, "commit") == 0) {
        amxc_var_t* data = dummy_read_json_from_file(UCI_COMMIT);
        amxc_var_move(ret, data);
        amxc_var_delete(&data);
    } else if(strcmp(method, "set") == 0) {
        const char* uci_set_exp_0 = "values:sendopts:60:softathome " \
            "76:192.168.0.253,192.168.0.254,proto:dhcp,clientid:0100BEEFC0FFEE," \
            "hostname:OpenWRT,ipaddr:192.168.0.1,classlessroute:0,reqopts:66 67 68," \
            "config:network,section:wan";
        const char* uci_set_exp_1 = "values:sendopts:60:softathome " \
            "76:192.168.0.253,192.168.0.254,proto:dhcp,reqopts:66 67 68," \
            "config:network,section:wan";
        char* uci_set = amxc_var_dyncast(cstring_t, args);
        if(amxb_call_set_cnt == 0) {
            assert_string_equal(uci_set_exp_0, uci_set);
        } else if(amxb_call_set_cnt == 1) {
            assert_string_equal(uci_set_exp_1, uci_set);
        } else {
            assert_string_equal("error", uci_set);
        }
        amxb_call_set_cnt++;
        free(uci_set);
    } else if(strcmp(method, "delete") == 0) {
        const char* uci_delete_exp[] = {
            "type:interface,option:classlessroute,config:network,section:wan",
            "type:interface,option:hostname,config:network,section:wan",
            "type:interface,option:ipaddr,config:network,section:wan",
            "type:interface,option:clientid,config:network,section:wan"
        };
        char* uci_delete = amxc_var_dyncast(cstring_t, args);
        if(amxb_call_delete_cnt < 4) {
            assert_string_equal(uci_delete_exp[amxb_call_delete_cnt], uci_delete);
        } else {
            assert_string_equal("error", uci_delete);
        }
        amxb_call_delete_cnt++;
        free(uci_delete);
    } else {
        char* uci_data = amxc_var_dyncast(cstring_t, args);
        printf("\nUnexpected amxb_call with method '%s', data '%s']", method, uci_data);
        fflush(stdout);
        free(uci_data);
        assert_true(0);
    }

    return 0;
}

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;

    assert_int_equal(amxm_so_open(&so, "target_module",
                                  "../modules_under_test/target_module.so"), 0);
    handle_events();
}

void test_dhcpv4c_start(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function("target_module", "mod-uci-access",
                                           "dhcpv4c-start", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    handle_events();
}

void test_dhcpv4c_stop(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function("target_module", "mod-uci-access",
                                           "dhcpv4c-stop", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    handle_events();
}
