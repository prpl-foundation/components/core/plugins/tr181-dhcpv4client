MACHINE = $(shell $(CC) -dumpmachine)

COMMON_SRCDIR = $(realpath ../../../common/src)
SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../../common/include_priv ../../include_priv ../include)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) \
		  $(wildcard $(COMMON_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -fPIC -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		   -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread -DUNIT_TEST

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxb -lamxc -lamxd -lamxj -lamxm -lamxo -lamxp \
		   -ldl -lsahtrace -ldhcpoptions -lpthread
