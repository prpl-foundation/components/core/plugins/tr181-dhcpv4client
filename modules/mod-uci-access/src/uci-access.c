/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

#include "uci-access.h"
#include "v4v6option.h"

const uci_option_keyword_t keys [] = {
    // Ordered by option_nr
    { "hostname", 12, true },
    { "ipaddr", 50, true },
    { "vendorid", 60, true },
    { "clientid", 61, false },
    { NULL, 0, false }
};

int uci_call(const char* method,
             const char* config,
             const char* section,
             const char* type,
             const char* option,
             amxc_var_t* values,
             amxc_var_t* result) {
    int status = 0;
    amxc_var_t args;
    amxb_bus_ctx_t* ctx = NULL;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    ctx = amxb_be_who_has("uci.");

    if(config != NULL) {
        amxc_var_add_key(cstring_t, &args, "config", config);
    }
    if(section != NULL) {
        amxc_var_add_key(cstring_t, &args, "section", section);
    }
    if(type != NULL) {
        amxc_var_add_key(cstring_t, &args, "type", type);
    }
    if(option != NULL) {
        amxc_var_add_key(cstring_t, &args, "option", option);
    }
    if(values) {
        amxc_var_add_key(amxc_htable_t,
                         &args,
                         "values",
                         amxc_var_constcast(amxc_htable_t, values));
    }
    status = amxb_call(ctx, "uci.", method, &args, result, 3);
    amxc_var_clean(&args);
    return status;
}

static bool uci_access_handle_send_keyword(amxc_var_t* data,
                                           uint8_t tag,
                                           const char* value,
                                           const char* uci_value) {
    bool is_keyword = false;
    uci_option_keyword_t* keyword = (uci_option_keyword_t*) keys;
    while(keyword->keyword != NULL) {
        if(tag == keyword->option_nr) {
            is_keyword = true;
            amxc_var_add_key(cstring_t, data, keyword->keyword,
                             keyword->convert ? uci_value : value);
            break;
        } else if(tag < keyword->option_nr) {
            // stop searching
            break;
        }
        keyword++;
    }
    return is_keyword;
}

static void uci_access_convert_to_uci_sent_option(amxc_var_t* args,
                                                  amxc_var_t* data) {
    bool first = true;
    amxc_string_t str_opts;
    amxc_var_t* tmp = NULL;

    amxc_string_init(&str_opts, 0);
    amxc_var_for_each(var, GETP_ARG(args, "SentOption")) {
        bool enabled = GET_BOOL(var, "Enable");
        uint8_t tag = (uint8_t) GET_UINT32(var, "Tag");
        uint32_t length = 0;
        const char* opt_value = GET_CHAR(var, "Value");
        unsigned char* option = NULL;
        char* uci_value = NULL;
        amxc_var_t* parsed_info = NULL;
        when_false_trace(enabled, end_loop, INFO, "Option %hhu is disabled", tag)
        when_str_empty_trace(opt_value, end_loop, INFO, "Option %hhu is empty", tag);
        option = dhcpoption_option_convert2bin(opt_value, &length);
        when_str_empty_trace(option, end_loop, WARNING, "Option %hhu, value '%s', " \
                             "hex conversion failed", tag, opt_value);
        amxc_var_new(&parsed_info);
        // Convert binary buffer to string(s) (amxc_var_t)
        dhcpoption_v4parse(parsed_info, tag, length, option);
        uci_value = amxc_var_dyncast(cstring_t, parsed_info);
        when_str_empty_trace(uci_value, skip_option, WARNING, "Option %hhu, value '%s', " \
                             "parsing failed", tag, opt_value);
        if(uci_access_handle_send_keyword(data, tag, opt_value,
                                          (const char*) uci_value) == false) {
            amxc_string_appendf(&str_opts, "%s%u:%s", first ? "" : " ", tag, uci_value);
            first = false;
        }
skip_option:
        free(uci_value);
        amxc_var_delete(&parsed_info);
end_loop:
        free(option);
    }
    tmp = amxc_var_add_new_key(data, "sendopts");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&str_opts));
    amxc_string_clean(&str_opts);
}

static void uci_access_convert_to_uci_request_option(amxc_var_t* args,
                                                     amxc_var_t* data) {
    bool first = true;
    amxc_string_t str_opts;
    amxc_var_t* tmp = NULL;

    amxc_string_init(&str_opts, 0);
    amxc_var_for_each(var, GETP_ARG(args, "ReqOption")) {
        uint32_t tag = GET_UINT32(var, "Tag");
        uint32_t tag_enabled = GET_UINT32(var, "Enable");
        if(tag == 121) {
            amxc_var_add_key(uint32_t, data, "classlessroute", tag_enabled);
        } else if(tag_enabled) {
            amxc_string_appendf(&str_opts, "%s%u", first ? "" : " ", tag);
            first = false;
        }
    }

    tmp = amxc_var_add_new_key(data, "reqopts");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&str_opts));

    amxc_string_clean(&str_opts);
}

void uci_access_convert_to_uci(amxc_var_t* args,
                               amxc_var_t* data) {
    bool enable = GET_BOOL(args, "Enable");

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    if(enable) {
        uci_access_convert_to_uci_sent_option(args, data);
        uci_access_convert_to_uci_request_option(args, data);
    }
}

/**
 * @brief  performs a ubus call uci delete
 *         '{"config":"network","type":"interface","section":section,"option":option}'
 */
static int uci_delete_network_option(const char* section, const char* option) {
    int status = 0;
    amxc_var_t uci_args;
    amxc_var_t uci_ret;
    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);

    status = uci_call("delete", "network", section, "interface", option, NULL, &uci_ret);
    SAH_TRACEZ_INFO(ME, "UCI delete interface '%s', option '%s' returned %d", section, option, status);
    if(status == 4) {
        // status code 4 happens when specified option was not in /etc/config/network
        status = 0;
    }

    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    return status;
}

int uci_access_delete_unused_option(UNUSED const char* section,
                                    amxc_var_t* data) {
    int retval = 0;
    const char* uci_option_names[] = {
        "classlessroute",
        "reqopts",
        "sendopts",
        "hostname",
        "ipaddr",
        "clientid",
        "vendorid"
    };
    for(size_t index = 0; index < (sizeof(uci_option_names) / sizeof(const char*)); index++) {
        if(amxc_var_get_key(data, uci_option_names[index], AMXC_VAR_FLAG_DEFAULT) == NULL) {
            // UCI option key is not used in current config, delete option in UCI config
            retval |= uci_delete_network_option(section, uci_option_names[index]);
        }
    }
    return retval;
}
