/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxb/amxb.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>

#include <amxm/amxm.h>

#include "mod-uci-access.h"
#include "uci-access.h"
#include "udhcpc.h"

#define UCI_SECTION_MAX_LENGTH 25
#define UCI_SECTION_NAME_IPv4 "UCISectionNameIPv4"
#define UCI_SECTION_WAN "wan"
typedef struct _uci_config {
    amxc_var_t* config_tree;
} _uci_config_t;

_uci_config_t app;

void update_state(UNUSED udhcpc_t* udhcpc);

static int get_uci_section(const char* interface, char* const uci_section_out) {

#ifdef UNIT_TEST
    strncpy(uci_section_out, UCI_SECTION_WAN, UCI_SECTION_MAX_LENGTH);
    return 0;
#endif

    int retval = 0;
    amxc_string_t rel_path;
    amxc_var_t data;
    const char* uci_section = NULL;
    amxb_bus_ctx_t* intf_ctx = NULL;

    amxc_string_init(&rel_path, 0);
    amxc_var_init(&data);

    when_str_empty_trace(interface, exit, ERROR, "Interface is null.");

    intf_ctx = amxb_be_who_has("IP.");
    when_null_trace(intf_ctx, exit, ERROR, "failed to find a bus context providing IP.");

    amxc_string_setf(&rel_path, "%s%s", interface, UCI_SECTION_NAME_IPv4);
    retval = amxb_get(intf_ctx, amxc_string_get(&rel_path, 0), 0, &data, 1);
    when_failed_trace(retval, exit, ERROR, "%s%s not found", interface, UCI_SECTION_NAME_IPv4);

    uci_section = GETP_CHAR(&data, "0.0."UCI_SECTION_NAME_IPv4);
    if(((uci_section) == NULL) || (*(uci_section) == 0)) {
        SAH_TRACEZ_WARNING(ME, UCI_SECTION_NAME_IPv4 " is empty. 'WAN' would be used as a default value.");
        uci_section = UCI_SECTION_WAN;
    }
    strncpy(uci_section_out, uci_section, UCI_SECTION_MAX_LENGTH);

    amxc_string_clean(&rel_path);
    amxc_var_clean(&data);
    return 0;

exit:
    amxc_string_clean(&rel_path);
    amxc_var_clean(&data);
    return -1;
}

/**
   @brief
   make an uci.commit call with parameter config="network"
   to make the configuration change effective

   @param function_name function name
   @param args argument, not used
   @param ret return value, variant int32_t with return value
              0 succes
              -1 failed

   @return 0 always
 */
static int update_config(UNUSED const char* function_name,
                         UNUSED amxc_var_t* args,
                         amxc_var_t* ret) {
    int retval = -1;

    retval = uci_call("commit", "network", NULL, NULL, NULL, NULL, ret);
    SAH_TRACEZ_INFO(ME, "UCI commit network returned - %d", retval);
    // uci.commit does not set ret variable
    amxc_var_set(int32_t, ret, retval);
    return 0;
}

/**
   @brief
   make an uci.set call with parameters config="network", section=Interface
   to make the configuration change effective

   @param function_name function name
   @param args argument
          { Interface, Enable, SentOption [ { Tag, Value, Enable },... ],
            ReqOption [ { Tag, Enable },... ] }, ifname
   @param ret return value, variant int32_t with return value
              0 succes
              -1 failed

   @return 0 always
 */
static int client_change(UNUSED const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    int retval = 0;
    char uci_section[UCI_SECTION_MAX_LENGTH];
    amxc_var_t data;
    amxc_var_init(&data);

    uci_access_convert_to_uci(args, &data);
    retval = get_uci_section(GET_CHAR(args, "Interface"), uci_section);
    when_failed_trace(retval, exit, ERROR, "Failed to find UCI section, interface = %s", uci_section);
    retval |= uci_call("set", "network", uci_section, NULL, NULL, &data, ret);
    SAH_TRACEZ_INFO(ME, "UCI set network, section %s returned - %d", uci_section, retval);
    retval |= uci_access_delete_unused_option(uci_section, &data);
    // uci.set does not set ret variable
    amxc_var_set(int32_t, ret, (retval == 0) ? 0 : -1);

    amxc_var_clean(&data);
    return 0;

exit:
    amxc_var_clean(&data);
    amxc_var_set(int32_t, ret, retval);
    return retval;
}

static int dhcpv4c_toggle(bool enable, amxc_var_t* args, amxc_var_t* ret) {
    int retval = -1;
    amxc_var_t data;
    char uci_section[UCI_SECTION_MAX_LENGTH];

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "proto", enable ? "dhcp" : "none");

    retval = get_uci_section(GET_CHAR(args, "Interface"), uci_section);
    when_failed_trace(retval, exit, ERROR, "Failed to find UCI section, interface = %s", uci_section);

    retval = uci_call("set", "network", uci_section, NULL, NULL, &data, ret);
    when_failed_trace(retval, exit, ERROR, "Failed to %s DHCPv4 on the %s interface", enable ? "enable" : "disable", uci_section);
    retval = uci_call("commit", "network", NULL, NULL, NULL, NULL, ret);

exit:
    amxc_var_clean(&data);
    amxc_var_set(int32_t, ret, retval);
    return retval;
}

static int dhcpv4c_start(UNUSED const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    return dhcpv4c_toggle(true, args, ret);
}

static int dhcpv4c_stop(UNUSED const char* function_name,
                        amxc_var_t* args,
                        amxc_var_t* ret) {
    return dhcpv4c_toggle(false, args, ret);
}

static int dummy_func(UNUSED const char* function_name,
                      UNUSED amxc_var_t* args,
                      amxc_var_t* ret) {
    amxc_var_set(int32_t, ret, 0);
    return 0;
}

void update_state(UNUSED udhcpc_t* udhcpc) {
    // Not used by mod-uci-access, it gets called in common code but only applicable for mod-udhcpc-access
}

static AMXM_CONSTRUCTOR uci_access_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "mod-uci-access %s", "start");

    amxc_var_new(&app.config_tree);
    amxc_var_set_type(app.config_tree, AMXC_VAR_ID_HTABLE);

    amxm_module_register(&mod, so, MOD_NAME);
    amxm_module_add_function(mod, "dhcpv4c-start", dhcpv4c_start);
    amxm_module_add_function(mod, "dhcpv4c-stop", dhcpv4c_stop);
    amxm_module_add_function(mod, "client-change", client_change);
    amxm_module_add_function(mod, "update-config", update_config);
    amxm_module_add_function(mod, "dhcpv4c-renew", dhcpv4c_renew);
    amxm_module_add_function(mod, "dhcpv4c-release", dhcpv4c_release);
    amxm_module_add_function(mod, "user-script", udhcpc_user_script);
    amxm_module_add_function(mod, "dhcpv4c-config-update", dummy_func);
    amxm_module_add_function(mod, "raw-sock-handler", dummy_func);
    amxm_module_add_function(mod, "dhcpv4c-get-statistics", dummy_func);
    amxm_module_add_function(mod, "dhcpv4c-clear-statistics", dummy_func);

    return 0;
}

static AMXM_DESTRUCTOR uci_access_uci_stop(void) {
    SAH_TRACEZ_INFO(ME, "mod-uci-access %s", "stop");
    amxc_var_delete(&app.config_tree);
    return 0;
}
