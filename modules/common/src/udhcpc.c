/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxm/amxm.h>

#include "udhcpc.h"
#include "v4v6option.h"

static amxc_string_t* udhcpc_add_comma(uint8_t opt, const char* value);
static amxc_string_t* udhcpc_no_change(uint8_t opt, const char* value);
static amxc_string_t* udhcpc_str2bin(uint8_t opt, const char* value);
static amxc_string_t* udhcpc_parse_static(uint8_t opt, const char* value);
static udhcpc_t* udhcpc_list_find_intf(const char* ifname);
void update_state(udhcpc_t* udhcpc);

typedef struct {
    const char* udhcpc_name;
    const char* dm_name;
    uint8_t option;
    amxc_string_t* (*convert)(uint8_t opt, const char* value);
} udhcpc_name_conv_t;

// keywords with double entry are mapped in datamodel and as request option
udhcpc_name_conv_t names[] = {
    { "ip", "IPAddress", 0, udhcpc_no_change },
    { "interface", "ifname", 0, udhcpc_no_change },
    { "siaddr", "DHCPServer", 0, udhcpc_no_change },
    { "subnet", "SubnetMask", 1, udhcpc_no_change },
    { "subnet", NULL, 1, udhcpc_str2bin },
    { "router", "IPRouters", 3, udhcpc_no_change },
    { "router", NULL, 3, udhcpc_str2bin },
    { "dns", "DNSServers", 6, udhcpc_add_comma },
    { "dns", NULL, 6, udhcpc_str2bin },
    { "lease", "LeaseTimeRemaining", 51, udhcpc_no_change },
    { "lease", NULL, 51, udhcpc_str2bin },
    { "timezone", NULL, 2, udhcpc_no_change },
    { "lprsrv", NULL, 9, udhcpc_str2bin },
    { "hostname", NULL, 12, udhcpc_str2bin },
    { "bootsize", NULL, 13, udhcpc_str2bin },
    { "domain", NULL, 15, udhcpc_str2bin },
    { "swapsrv", NULL, 16, udhcpc_str2bin },
    { "rootpath", NULL, 17, udhcpc_str2bin },
    { "ipttl", NULL, 23, udhcpc_str2bin },
    { "mtu", NULL, 26, udhcpc_str2bin },
    { "broadcast", NULL, 28, udhcpc_str2bin },
    { "routes", NULL, 33, udhcpc_str2bin },
    { "nisdomain", NULL, 40, udhcpc_str2bin },
    { "nissrv", NULL, 41, udhcpc_str2bin },
    { "ntpsrv", NULL, 42, udhcpc_str2bin },
    { "wins", NULL, 44, udhcpc_str2bin },
    { "serverid", NULL, 54, udhcpc_str2bin },
    { "message", NULL, 56, udhcpc_str2bin },
    { "vendor", NULL, 60, udhcpc_str2bin },
    { "tftp", NULL, 66, udhcpc_str2bin },
    { "bootfile", NULL, 67, udhcpc_str2bin },
    { "boot_file", NULL, 67, udhcpc_str2bin },
    { "tzstr", NULL, 100, udhcpc_str2bin },
    { "tzdbstr", NULL, 101, udhcpc_str2bin },
    { "search", NULL, 119, udhcpc_str2bin },
    { "sipsrv", NULL, 120, udhcpc_str2bin },
    { "staticroutes", NULL, 121, udhcpc_parse_static },
    { "vlanid", NULL, 132, udhcpc_str2bin },
    { "vlanpriority", NULL, 133, udhcpc_str2bin },
    { "pxeconffile", NULL, 209, udhcpc_str2bin },
    { "pxepathprefix", NULL, 210, udhcpc_str2bin },
    { "reboottime", NULL, 211, udhcpc_str2bin },
    { "ip6rd", NULL, 212, udhcpc_str2bin },
    { "msstaticroutes", NULL, 249, udhcpc_parse_static },
    { "wpad", NULL, 252, udhcpc_str2bin },
    { NULL, NULL, 0, NULL }
};

amxc_llist_t* if_list = NULL;


static amxc_string_t* udhcpc_no_change(UNUSED uint8_t opt,
                                       const char* value) {
    amxc_string_t* str_value = NULL;
    amxc_string_new(&str_value, 0);
    amxc_string_set(str_value, value);

    return str_value;
}

static amxc_string_t* udhcpc_add_comma(UNUSED uint8_t opt,
                                       const char* value) {
    amxc_string_t* str_value = NULL;
    amxc_string_new(&str_value, 0);
    amxc_string_set(str_value, value);
    amxc_string_replace(str_value, " ", ",", UINT32_MAX);

    return str_value;
}

/* WARNING
 * this function un parse the parsing of udhcpc that is part of busybox
 * IN : "255.255.255.255/32 255.255.255.255 "
 * OUT : "20FFFFFFFFFFFFFFFF"
 */
static amxc_string_t* udhcpc_parse_static(UNUSED uint8_t opt,
                                          const char* value) {
    uint8_t bytes[9];
    int length = 0;
    int offset = 0;
    char* encoded_value = NULL;
    char* tmp = NULL;
    amxc_string_t* str_value = NULL;
    char* cpy = strdup(value);
    char* token = strtok(cpy, " ");

    amxc_string_new(&str_value, 0);

    while(token != NULL) {
        int nb_bytes = 0;
        int ret = 0;

        ret = sscanf(token, "%hhu.%hhu.%hhu.%hhu/%hhu", &bytes[1], &bytes[2], &bytes[3], &bytes[4], &bytes[0]);
        when_false_trace(ret != EOF, exit, ERROR, "No bytes written");
        token = strtok(NULL, " ");

        when_null_trace(token, exit, ERROR, " data mal formated %s", value);
        ret = sscanf(token, "%hhd.%hhd.%hhd.%hhd ", &bytes[5], &bytes[6], &bytes[7], &bytes[8]);
        when_false_trace(ret != EOF, exit, ERROR, "No bytes written");

        nb_bytes = (bytes[0] + 7) / 8;

        length += ( nb_bytes + 1 + 4 );
        tmp = encoded_value;
        encoded_value = realloc(encoded_value, length);
        when_null_trace(encoded_value, exit, ERROR, "Unable to allocate new memory");
        tmp = NULL;
        encoded_value[offset] = bytes[0];
        memcpy(&encoded_value[offset + 1], &bytes[1], nb_bytes);
        memcpy(&encoded_value[offset + 1 + nb_bytes], &bytes[5], 4);
        offset = length;
        token = strtok(NULL, " ");
    }

    amxc_string_bytes_2_hex_binary(str_value, encoded_value, length, NULL);

exit:
    free(cpy);
    free(encoded_value);
    if(tmp != NULL) { // Realloc failed, free previously allocated memory
        free(tmp);
    }
    return str_value;
}

static amxc_string_t* udhcpc_str2bin(uint8_t opt,
                                     const char* value) {
    uint32_t length = strlen(value);
    const char* encoded_value = NULL;
    amxc_string_t* str_value = NULL;

    amxc_string_new(&str_value, 0);
    // use lib_dhcpoptions to convert value
    encoded_value = (const char*) dhcpoption_v4_str2bin(opt, value, &length);
    amxc_string_bytes_2_hex_binary(str_value, encoded_value, length, NULL);

    return str_value;
}

static udhcpc_t* udhcpc_list_find_intf(const char* ifname) {
    udhcpc_t* ret_ptr = NULL;
    const char* used_ifname = NULL;

    when_null(ifname, exit);
    when_null(if_list, exit);
    amxc_llist_for_each(it, if_list) {
        udhcpc_t* udhcp = amxc_container_of(it, udhcpc_t, it);
        used_ifname = GET_CHAR(udhcp->used_args, "ifname");
        if((used_ifname != NULL) && (strcmp(ifname, used_ifname) == 0)) {
            ret_ptr = udhcp;
            break;
        }
    }
exit:
    return ret_ptr;
}

static bool udhcpc_key_found (udhcpc_name_conv_t** key_found,
                              const char* key) {
    bool found = false;
    udhcpc_name_conv_t* key_name = names;
    while(key_name->udhcpc_name != NULL) {
        if(strcmp(key, key_name->udhcpc_name) == 0) {
            *key_found = key_name;
            found = true;
            break;
        }
        key_name++;
    }
    return found;
}

int udhcpc_user_script(const char* function_name,
                       amxc_var_t* args,
                       amxc_var_t* ret) {
    (void) function_name; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Function called - %s", function_name);

    int retval = -1;
    udhcpc_t* udhcpc;
    amxc_var_t data;
    amxc_var_t* params = NULL;
    amxc_var_t* var_opts = NULL;
    const char* action = NULL;
    const char* ifname = NULL;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    // parameters action and ifname are obligatory
    when_null_trace(args, exit, ERROR, "Bad input args parameter");
    action = GET_CHAR(args, "action");
    ifname = GET_CHAR(args, "interface");
    if(((action == NULL) || (*action == '\0')) ||
       ((ifname == NULL) || (*ifname == '\0'))) {
        amxc_var_set(cstring_t, ret, "error_invalid_parameter");
        goto exit;
    }

    params = amxc_var_add_key(amxc_htable_t, &data, "parameters", NULL);
    var_opts = amxc_var_add_key(amxc_llist_t, params, "ReqOption", NULL);

    if(strcmp(action, "deconfig") == 0) {
        amxc_var_add_key(bool, params, "bound", false);
    } else if((strcmp(action, "renew") == 0) ||
              (strcmp(action, "bound") == 0)) {
        amxc_var_add_key(bool, params, "bound", true);
    }

    amxc_var_for_each(var, args) {
        udhcpc_name_conv_t* key_name = NULL;
        const char* key = amxc_var_key(var);
        const char* value = amxc_var_constcast(cstring_t, var);
        uint32_t option_nr = 0;
        char dummy_char = 0;
        if(sscanf(key, "opt%u%c", &option_nr, &dummy_char) == 1) {
            // udhcpc component gives values as hexbinary strings, no conversion needed
            amxc_var_t* option = amxc_var_add(amxc_htable_t, var_opts, NULL);
            amxc_var_add_key(uint8_t, option, "Tag", option_nr);
            amxc_var_add_key(cstring_t, option, "Value", value);
        } else if(udhcpc_key_found(&key_name, key)) {
            if(key_name->dm_name != NULL) {
                amxc_string_t* str_val = NULL;
                str_val = key_name->convert(0, value);
                if(str_val != NULL) {
                    amxc_var_add_key(cstring_t, params, key_name->dm_name,
                                     amxc_string_get(str_val, 0));
                    amxc_string_delete(&str_val);
                }
                if(key_name->option > 0) {
                    // Check next entry in table
                    option_nr = key_name->option;
                    key_name++;
                    if(option_nr != key_name->option) {
                        SAH_TRACEZ_WARNING(ME, "No double entry of '%s' " \
                                           "found in table 'names'", key);
                        continue;
                    }
                }
            }
            if(key_name->option > 0) {
                amxc_string_t* str_val = NULL;
                amxc_var_t* option = amxc_var_add(amxc_htable_t, var_opts, NULL);
                str_val = key_name->convert(key_name->option, value);
                amxc_var_add_key(uint8_t, option, "Tag", key_name->option);
                amxc_var_add_key(cstring_t, option, "Value",
                                 amxc_string_get(str_val, 0));
                amxc_string_delete(&str_val);
            }
        }
    }
    retval = amxm_execute_function("self", MOD_CORE, "update-client", &data, ret);
    if(retval != 0) {
        // if module exists, function should be implemented
        SAH_TRACEZ_WARNING(ME, "Mod %s, func 'update-client' returned %d",
                           MOD_CORE, retval);
        goto exit;
    }

    udhcpc = udhcpc_list_find_intf(ifname);
    if((udhcpc != NULL) && (strcmp(action, "started") == 0)) {
        udhcpc->state = ODHCP_STARTED;
        update_state(udhcpc);
    }

exit:
    amxc_var_clean(&data);
    return 0;
}

int udhcpc_get_pid(const char* ifname) {
    amxc_string_t pid_file_str;
    const char* fname = NULL;
    FILE* file = NULL;
    int pid = -1;

    amxc_string_init(&pid_file_str, 0);
    amxc_string_setf(&pid_file_str, "/var/run/udhcpc-%s.pid",
                     ifname);
    fname = amxc_string_get(&pid_file_str, 0);

    file = fopen(fname, "r");
    if(file == NULL) {
        SAH_TRACEZ_ERROR(ME, "Error opening file %s - error", fname);
        goto exit;
    }
    if(fscanf(file, "%d", &pid) != 1) {
        SAH_TRACEZ_ERROR(ME, "Error getting %s", "pid");
    }
    fclose(file);
exit:
    amxc_string_clean(&pid_file_str);
    return pid;
}

void udhcpc_notify(amxc_var_t* args,
                   amxc_var_t* ret,
                   bool renew) {
    int pid = 0;
    int retval = -1;
    const char* ifname = GET_CHAR(args, NULL);

    pid = udhcpc_get_pid(ifname);
    if(pid > 0) {
        retval = kill(pid, renew ? SIGUSR1 : SIGUSR2);
    }
    if(retval != 0) {
        if(pid > 0) {
            SAH_TRACEZ_ERROR(ME, "Unable to send SIGUSR%d to udhcpc (pid %d)" \
                             " for %s, kill returned error",
                             renew ? 1 : 2, pid, ifname);
        } else {
            SAH_TRACEZ_ERROR(ME, "Invalid pid number (%d), not able to send" \
                             " SIGUSR%d to udhcpc for %s",
                             pid, renew ? 1 : 2, ifname);
        }
    } else {
        SAH_TRACEZ_INFO(ME, "Sent SIGUSR%d to udhcpc(pid %d) (for ifname %s)",
                        renew ? 1 : 2, pid, ifname);
    }

    amxc_var_set(int32_t, ret, retval);
}

/**
   @brief
   Inform process udhcpc with signal SIGUSR1 to start a DHCPv4 renew

   @param function_name function name
   @param args argument, variant string with ifname

   @param ret return value, variant int32_t with return value
              0 succes
              -1 failed

   @return 0 always
 */
int dhcpv4c_renew(const char* function_name,
                  amxc_var_t* args,
                  amxc_var_t* ret) {
    (void) function_name; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Function called - %s", function_name);
    // Send SIGUSR1 to udhcpc
    udhcpc_notify(args, ret, true);
    return 0;
}

/**
   @brief
   Inform process udhcpc with signal SIGUSR2 to stop DHCPv4 client

   @param function_name function name
   @param args argument, variant string with ifname

   @param ret return value, variant int32_t with return value
              0 succes
              -1 failed

   @return 0 always
 */
int dhcpv4c_release(const char* function_name,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    (void) function_name; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Function called - %s", function_name);
    // Send SIGUSR2 to udhcpc
    udhcpc_notify(args, ret, false);
    return 0;
}
